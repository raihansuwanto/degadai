package model

import (
	"time"

	"gorm.io/gorm"
)

type Ticket struct {
	gorm.Model
	ID          uint
	AreaId      uint      `json:"area_id"`
	Name        string    `json:"name"`
	Image       string    `json:"image"`
	Description string    `json:"description"`
	Stock       int64     `json:"stock"`
	DateStart   time.Time `json:"date_start"`
	DateEnd     time.Time `json:"date_end"`

	Shows *[]Shows `gorm:"foreignKey:ticket_id" json:"shows"`
	// OrderedStock int64  `json:"ordered_stock"`
}
