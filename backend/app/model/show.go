package model

import (
	"time"

	"gorm.io/gorm"
)

type Shows struct {
	gorm.Model
	ID             uint
	AreaId         uint        `json:"area_id"`
	TheaterId      uint        `json:"theater_id"`
	TheaterTimesId uint        `json:"theater_times_id"`
	TicketId       uint        `json:"ticket_id"`
	Date           time.Time   `gorm:"type:time" json:"date"`
	TheaterTime    TheaterTime `gorm:"foreignKey:theater_times_id" json:"theater_time"`
	// Ticket         Ticket      `gorm:"foreignKey:ticket_id" json:"ticket"`
}
