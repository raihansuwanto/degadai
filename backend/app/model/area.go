package model

import (
	"gorm.io/gorm"
)

type Area struct {
	gorm.Model
	ID   uint
	Name string `json:"name"`
	// OrderedStock int64  `json:"ordered_stock"`
}
