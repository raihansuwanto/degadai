package model

import (
	"gorm.io/gorm"
)

type Order struct {
	gorm.Model
	UserId   int    `json:"user_id"`
	TicketId int    `json:"ticket_id"`
	User     User   `json:"user" gorm:"foreignKey:UserId"`
	Ticket   Ticket `json:"ticket" gorm:"foreignKey:TicketId"`
}
