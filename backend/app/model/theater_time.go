package model

import (
	"gorm.io/gorm"
)

type TheaterTime struct {
	gorm.Model
	ID        uint
	TheaterId uint   `json:"theater_id"`
	TimeStart string `gorm:"type:time" json:"time_start"`
	TimeEnd   string `gorm:"type:time" json:"time_end"`
}
