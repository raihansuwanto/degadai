package model

import (
	"fmt"

	"github.com/satori/uuid"
	"gorm.io/gorm"
)

// User represent a user account in application
type User struct {
	gorm.Model
	Serial   string `json:"-"`
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"-"`
	Address  string `json:"address"`
	IsAdmin  bool   `json:"is_admin"`
	// Photo    string
}

// BeforeCreate gorm hooks, populate serial with uuid
func (u *User) BeforeCreate(tx *gorm.DB) (err error) {
	u.Serial = fmt.Sprintf("usr-%s", uuid.NewV4().String())
	return
}
