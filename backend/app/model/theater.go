package model

import (
	"gorm.io/gorm"
)

type Theater struct {
	gorm.Model
	ID          uint
	AreaId      uint    `json:"area_id"`
	Name        string  `json:"name"`
	Telp        string  `json:"telp"`
	Description string  `json:"description"`
	Address     string  `json:"address"`
	Shows       []Shows `gorm:"foreignKey:theater_id" json:"shows"`
	Area        *Area   `json:"area"`
}
