package model

import (
	"gorm.io/gorm"
)

// Customer represent a customer account in application
type Customer struct {
	gorm.Model
	ID       uint
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string
	Address  string `json:"address"`
}
