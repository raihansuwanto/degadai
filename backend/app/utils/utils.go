package utils

import (
	"strconv"
)

// StringToUint convert string to uint or zero if parsing failed
func StringToUint(str string) uint {
	parsedInt, err := strconv.ParseUint(str, 10, 32)
	if err != nil {
		return 0
	}

	return uint(parsedInt)
}

func StringToInt64(str string) int64 {
	if parsed, err := strconv.ParseInt(str, 10, 64); err == nil {
		return parsed
	}
	return 0
}
