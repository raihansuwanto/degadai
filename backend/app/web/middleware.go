package web

import (
	"fmt"
	"net/http"
	"strings"

	"starter.co/api/framework"

	"github.com/gin-gonic/gin"
	"github.com/lestrrat-go/jwx/jwt"
	"github.com/sirupsen/logrus"
	"starter.co/api/app/data"
	"starter.co/api/app/model"
	"starter.co/api/app/utils"
)

// AuthenticateUserObjectKey key for authenticated user
const AuthenticateUserObjectKey = "auth-user"

// VerifiedTokenObjectKey key for accessing verified jwt result
const VerifiedTokenObjectKey = "verified-token"

// GetAuthenticatedUser return authenticated user from request
func GetAuthenticatedUser(ctx *gin.Context) *model.User {
	authenticatedUser := ctx.Value(AuthenticateUserObjectKey)
	if authenticatedUser == nil {
		return nil
	}
	return authenticatedUser.(*model.User)
}

// GetVerifiedToken return verified jwt result from request
func GetVerifiedToken(ctx *gin.Context) jwt.Token {
	token := ctx.Value(VerifiedTokenObjectKey)
	if token == nil {
		return nil
	}
	return token.(jwt.Token)
}

// JWTAuthentication return jwt auth gin middleware
func JWTAuthentication(userRepo data.UserRepo, verif *framework.JWTVerifier) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		authHeader := strings.TrimSpace(ctx.GetHeader("Authorization"))
		bearerTagLen := len("Bearer")

		if authHeader == "" || len(authHeader) < bearerTagLen {
			ctx.AbortWithStatusJSON(http.StatusBadRequest, errorResp("authorization header not provided"))
			return
		}

		tokenString := authHeader[bearerTagLen:]
		token, err := verif.VerifyJWT(tokenString)
		if err != nil {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, errorResp("token verification failed"))
			return
		}

		userID, ok := token.Get("uid")
		if !ok {
			ctx.AbortWithStatusJSON(http.StatusUnprocessableEntity, errorResp("invalid provided token"))
			return
		}

		user, err := userRepo.FindByID(utils.StringToUint(fmt.Sprintf("%s", userID)))
		if err != nil || user == nil {
			logrus.Debugf("user not found %s", userID)
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, errorResp("user not found"))
			return
		}

		ctx.Set(VerifiedTokenObjectKey, token)
		ctx.Set(AuthenticateUserObjectKey, user)
		ctx.Next()
	}
}
