package web

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"starter.co/api/app/provider"
)

type areaHandler struct {
	ctx *provider.Context
}

func (h *areaHandler) IndexArea(c *gin.Context) {

	data, err := h.ctx.AreaRepo.Index()
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResp("error add new ticket"))
		return
	}

	c.JSON(http.StatusOK, successResp("retrieve area data success", data))
}
