package web

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"starter.co/api/app/model"
	"starter.co/api/app/provider"
)

type theaterHandler struct {
	ctx *provider.Context
}

func (h *theaterHandler) IndexTheater(c *gin.Context) {

	var req struct {
		Name   string `form:"name" json:"name"`
		AreaId int64  `form:"area_id" json:"area_id"`
	}

	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error()))
		return
	}

	data, err := h.ctx.TheaterRepo.Index(req.AreaId, req.Name)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResp("error retrieve theaters"))
		return
	}

	c.JSON(http.StatusOK, successResp("retrieve theaters data success", data))
}

func (h *theaterHandler) showTheater(c *gin.Context) {
	id := c.Param("id")

	theaterID, _ := strconv.ParseInt(id, 0, 64)
	data, err := h.ctx.TheaterRepo.FindByID(uint(theaterID))
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResp("error Theaters Not Found"))
		return
	}

	c.JSON(http.StatusOK, successResp("retrieve theater data success", data))
}

func (h *theaterHandler) AddTheater(c *gin.Context) {

	user := GetAuthenticatedUser(c)
	if !user.IsAdmin {
		c.JSON(http.StatusBadRequest, errorResp("You dont have access to make this request."))
		return
	}

	var req struct {
		Name        string `form:"name" json:"name" binding:"required"`
		Description string `form:"description" json:"description"`
		Address     string `form:"address" json:"address" binding:"required"`
		Telp        string `form:"telp" json:"telp" binding:"required"`
		AreaId      int64  `form:"area_id" json:"area_id" binding:"required"`
	}

	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error()))
		return
	}

	theater := &model.Theater{
		Name:        req.Name,
		Description: req.Description,
		Address:     req.Address,
		AreaId:      uint(req.AreaId),
		Telp:        req.Telp,
	}

	datErr := h.ctx.TheaterRepo.Add(theater)
	if datErr != nil {
		c.JSON(http.StatusBadRequest, errorResp("error add new ticket"))
		return
	}

	c.JSON(http.StatusOK, successResp("add new ticket success", nil))
}
