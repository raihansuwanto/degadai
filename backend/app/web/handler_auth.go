package web

import (
	"net/http"

	"starter.co/api/framework"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"starter.co/api/app/model"
	"starter.co/api/app/provider"
)

type registerRequest struct {
	Name     string `form:"name" json:"name" binding:"required"`
	Email    string `form:"email" json:"email" binding:"required,email"`
	Password string `form:"password" json:"password" binding:"required"`
	Address  string `form:"address" json:"address" binding:"required"`
}

type loginRequest struct {
	Email    string `form:"email" json:"email" binding:"required,email"`
	Password string `form:"password" json:"password" binding:"required"`
}

type changePasswordRequest struct {
	OldPassword string `form:"old_password" json:"old_password" binding:"required"`
	NewPassword string `form:"new_password" json:"new_password" binding:"required"`
}

type loginResponse struct {
	ID           uint   `json:"user_id"`
	Name         string `json:"name"`
	Email        string `json:"email"`
	IsAdmin      bool   `json:"is_admin"`
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

type authHandler struct {
	ctx    *provider.Context
	jwtGen *JWTGenerator
}

func (h *authHandler) registerCustomer(c *gin.Context) {
	var req registerRequest
	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error()))
		return
	}

	hashed, err := framework.HashPassword(req.Password)
	if err != nil {
		c.JSON(http.StatusInternalServerError, errorResp(err.Error()))
		return
	}
	user := &model.User{
		Name:     req.Name,
		Email:    req.Email,
		Password: hashed,
		IsAdmin:  false,
		Address:  req.Address,
	}

	if err := h.ctx.UserRepo.Add(user); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusInternalServerError, errorResp("error creating new user"))
		return
	}

	accessToken, refreshToken, err := h.jwtGen.GenerateNew(user.ID)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, errorResp("can not generate user tokens"))
		return
	}

	resp := loginResponse{
		ID:           user.ID,
		Name:         user.Name,
		Email:        user.Email,
		IsAdmin:      user.IsAdmin,
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}
	c.JSON(http.StatusOK, successResp("register success", resp))
}

func (h *authHandler) login(c *gin.Context) {
	var req loginRequest
	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error()))
		return
	}

	user, err := h.ctx.UserRepo.FindByEmail(req.Email)
	if err != nil || user == nil {
		c.JSON(http.StatusBadRequest, errorResp("error or no such user found"))
		return
	}

	if !framework.CheckPasswordHash(req.Password, user.Password) {
		c.JSON(http.StatusBadRequest, errorResp("credentials doesn't match"))
		return
	}

	accessToken, refreshToken, err := h.jwtGen.GenerateNew(user.ID)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, errorResp("can not generate user tokens"))
		return
	}

	resp := loginResponse{
		ID:           user.ID,
		Name:         user.Name,
		Email:        user.Email,
		IsAdmin:      user.IsAdmin,
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}
	c.JSON(http.StatusOK, successResp("login success", resp))
}

func (h *authHandler) SelfUpdate(c *gin.Context) {

	user := GetAuthenticatedUser(c)

	var req struct {
		ID      int
		Name    string `form:"name" json:"name"`
		Email   string `form:"email" json:"email"`
		Address string `form:"address" json:"address"`
	}

	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error()))
		return
	}

	input := &model.User{
		Name:    req.Name,
		Email:   req.Email,
		Address: req.Address,
	}

	err := h.ctx.UserRepo.Update(uint(user.ID), input)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResp("error update customer"))
		return
	}

	c.JSON(http.StatusOK, successResp("update customer success", nil))
}

func (h *authHandler) changePassword(c *gin.Context) {
	var req changePasswordRequest
	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error()))
		return
	}

	user := GetAuthenticatedUser(c)
	if framework.CheckPasswordHash(req.OldPassword, user.Password) {
		hashedNewPassword, _ := framework.HashPassword(req.NewPassword)
		unitOfWork := h.ctx.Transactional.CreateUnitOfWork()
		if err := unitOfWork.GetUserRepo().ChangePassword(user.ID, hashedNewPassword); err != nil {
			unitOfWork.Rollback()
			c.JSON(http.StatusInternalServerError, errorResp("error updating new password"))
			return
		}
		unitOfWork.Complete()
		c.JSON(http.StatusOK, successResp("password updated", nil))
	} else {
		c.JSON(http.StatusBadRequest, errorResp("old password doesn't match"))
	}
}

type newAccessTokenResponse struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

func (h *authHandler) refreshAccessToken(c *gin.Context) {
	currentRefreshToken := GetVerifiedToken(c)
	user := GetAuthenticatedUser(c)
	accessToken, refreshToken, err := h.jwtGen.RefreshAccessToken(user.ID, currentRefreshToken)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, errorResp("can not refresh new access token"))
	} else {
		c.JSON(http.StatusOK, successResp("success", newAccessTokenResponse{
			AccessToken:  accessToken,
			RefreshToken: refreshToken,
		}))
	}
}
