package web

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"starter.co/api/app/model"
	"starter.co/api/app/provider"
)

type customerHandler struct {
	ctx *provider.Context
}

func (h *customerHandler) IndexCustomer(c *gin.Context) {

	name := c.Query("name")

	if name != "" {
		var req struct {
			Name string `form:"name" json:"name"`
		}

		if err := c.ShouldBind(&req); err != nil {
			logrus.Error(err.Error())
			c.JSON(http.StatusBadRequest, errorResp(err.Error()))
			return
		}

		data, err := h.ctx.UserRepo.FindByName(req.Name)
		if err != nil {
			c.JSON(http.StatusBadRequest, errorResp("error add new customer"))
			return
		}

		c.JSON(http.StatusOK, successResp("retrieve customer data success", data))
		return
	}

	data, err := h.ctx.UserRepo.Index()
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResp("error add new customer"))
		return
	}

	c.JSON(http.StatusOK, successResp("retrieve customer data success", data))
}

func (h *customerHandler) AddCustomer(c *gin.Context) {
	var req struct {
		Name     string `form:"name" json:"name" binding:"required"`
		Email    string `form:"email" json:"email" binding:"required"`
		Password string `form:"password" json:"password" binding:"required"`
		Address  string `form:"address" json:"address" binding:"required"`
	}

	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error()))
		return
	}

	customer := &model.Customer{
		Name:     req.Name,
		Email:    req.Email,
		Password: req.Password,
		Address:  req.Address,
	}

	err := h.ctx.CustomerRepo.Add(customer)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResp("error add new customer"))
		return
	}

	c.JSON(http.StatusOK, successResp("add new customer success", nil))
}

func (h *customerHandler) UpdateCustomer(c *gin.Context) {
	id := c.Param("id")
	customerID, _ := strconv.ParseInt(id, 0, 64)

	var req struct {
		ID       int
		Name     string `form:"name" json:"name"`
		Email    string `form:"email" json:"email"`
		Password string `form:"password" json:"password"`
		Address  string `form:"address" json:"address"`
	}

	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error()))
		return
	}

	input := &model.Customer{
		ID:       uint(customerID),
		Name:     req.Name,
		Email:    req.Email,
		Password: req.Password,
		Address:  req.Address,
	}

	err := h.ctx.CustomerRepo.Update(input)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResp("error update customer"))
		return
	}

	c.JSON(http.StatusOK, successResp("update customer success", nil))
}

func (h *customerHandler) FindCustomer(c *gin.Context) {
	id := c.Param("id")
	customerID, _ := strconv.ParseInt(id, 0, 64)
	data, err := h.ctx.CustomerRepo.FindByID(uint(customerID))
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResp("error Customer Not Found"))
		return
	}

	c.JSON(http.StatusOK, successResp("retrieve customer data success", data))
}

func (h *customerHandler) SearchCustomerByName(c *gin.Context) {
	var req struct {
		Name string `form:"name" json:"name" binding:"required"`
	}

	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error()))
		return
	}

	data, err := h.ctx.CustomerRepo.FindByName(req.Name)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResp("error add new customer"))
		return
	}

	c.JSON(http.StatusOK, successResp("retrieve customer data success", data))
}
