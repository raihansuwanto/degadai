package web

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"starter.co/api/app/model"
	"starter.co/api/app/provider"
)

type orderHandler struct {
	ctx *provider.Context
}

func (h *orderHandler) IndexOrder(c *gin.Context) {

	userID := c.Query("user_id")
	ticketID := c.Query("ticket_id")

	if userID != "" || ticketID != "" {
		ticketID, _ := strconv.ParseInt(ticketID, 0, 64)
		userID, _ := strconv.ParseInt(userID, 0, 64)
		data, err := h.ctx.OrderRepo.Query(int(userID), int(ticketID))
		if err != nil {
			c.JSON(http.StatusBadRequest, errorResp("error show index order"))
			return
		}

		c.JSON(http.StatusOK, successResp("retrieve order data success", data))
		return
	}

	data, err := h.ctx.OrderRepo.Index()

	if err != nil {
		c.JSON(http.StatusBadRequest, errorResp("error show index order"))
		return
	}

	c.JSON(http.StatusOK, successResp("retrieve order data success", data))
}

func (h *orderHandler) AddOrder(c *gin.Context) {

	user := GetAuthenticatedUser(c)
	if user.IsAdmin {
		c.JSON(http.StatusBadRequest, errorResp("You dont have access to make this request."))
		return
	}

	id := c.Param("id")
	ticketID, _ := strconv.ParseInt(id, 0, 64)
	ticket, err := h.ctx.TicketRepo.FindByID(uint(ticketID))

	if err != nil {
		c.JSON(http.StatusBadRequest, errorResp("error ticket Not Found"))
		return
	}

	isAlreadyBooked := h.ctx.OrderRepo.CountByUserIdTicketId(ticketID, int64(user.ID))

	if isAlreadyBooked > 0 {
		c.JSON(http.StatusBadRequest, errorResp("ticket already booked"))
		return
	}

	countBookedTicket := h.ctx.OrderRepo.CountByTicketId(ticketID)
	if countBookedTicket >= ticket.Stock {
		c.JSON(http.StatusBadRequest, errorResp("ticket unavailable"))
		return
	}

	input := &model.Order{
		UserId:   int(user.ID),
		TicketId: int(ticketID),
	}

	err = h.ctx.OrderRepo.Add(input)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResp("error add new order"))
		return
	}

	c.JSON(http.StatusOK, successResp("add new order success", nil))
}
