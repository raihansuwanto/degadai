package web

type baseResponse struct {
	Success bool        `json:"success"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
	Error   *errorData  `json:"error"`
}

type errorData struct {
	Message string `json:"message"`
}

func errorResp(errorMessage string) baseResponse {
	return baseResponse{
		Success: false,
		Message: "",
		Data:    nil,
		Error: &errorData{
			Message: errorMessage,
		},
	}
}

func successResp(message string, data interface{}) baseResponse {
	return baseResponse{
		Success: true,
		Message: message,
		Data:    data,
		Error:   nil,
	}
}
