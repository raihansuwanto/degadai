package web

import (
	"net/http"
	"time"

	"starter.co/api/framework/messaging"

	"starter.co/api/app/job"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"starter.co/api/app/provider"
	"starter.co/api/config"
)

// InitHTTPRouters initialize http routes handlers
func InitHTTPRouters(ctx *provider.Context, cfg *config.Config, engine *gin.Engine) {
	engine.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"DELETE", "GET", "POST", "PUT"},
		AllowHeaders:     []string{"Authorization", "Origin", "Content-Length", "Content-Type", "Range"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	authHandler := authHandler{
		ctx: ctx,
		jwtGen: &JWTGenerator{
			accessTokenVerif:  ctx.AccessTokenVerifier,
			refreshTokenVerif: ctx.RefreshTokenVerifier,
		},
	}
	customerHandler := customerHandler{
		ctx: ctx,
	}

	orderHandler := orderHandler{
		ctx: ctx,
	}

	ticketHandler := ticketHandler{
		ctx: ctx,
	}

	areaHandler := areaHandler{
		ctx: ctx,
	}

	theaterHandler := theaterHandler{
		ctx: ctx,
	}

	engine.StaticFS("public/files", http.Dir(cfg.LocalStorage.PublicBaseDir))

	publicAPI := engine.Group("/api")
	publicAPI.POST("/auth/register", authHandler.registerCustomer)
	publicAPI.POST("/auth/login", authHandler.login)

	publicAPI.POST("/customer", customerHandler.AddCustomer)
	publicAPI.GET("/customer", customerHandler.IndexCustomer)
	publicAPI.GET("/customer/:id", customerHandler.FindCustomer)
	publicAPI.PUT("/customer/:id", customerHandler.UpdateCustomer)
	// publicAPI.GET("/customer/search", customerHandler.SearchCustomerByName)

	refreshTokenAPI := engine.Group("/api")
	refreshTokenAPI.Use(JWTAuthentication(ctx.UserRepo, ctx.RefreshTokenVerifier))
	refreshTokenAPI.GET("/auth/refresh", authHandler.refreshAccessToken)

	guardedAPI := engine.Group("/api")
	guardedAPI.Use(JWTAuthentication(ctx.UserRepo, ctx.AccessTokenVerifier))

	guardedAPI.POST("/ticket", ticketHandler.AddTicket)
	guardedAPI.PUT("/ticket/:id", ticketHandler.UpdateTicket)
	guardedAPI.DELETE("/ticket/:id", ticketHandler.DeleteTicket)

	publicAPI.GET("/ticket", ticketHandler.IndexTicket)
	publicAPI.GET("/ticket-public", ticketHandler.IndexPublic)

	publicAPI.GET("/ticket/:id", ticketHandler.FindTicket)

	publicAPI.GET("/ticket-images", ticketHandler.getTicketImages)
	publicAPI.GET("/area", areaHandler.IndexArea)

	guardedAPI.POST("/theater", theaterHandler.AddTheater)
	publicAPI.GET("/theater", theaterHandler.IndexTheater)
	publicAPI.GET("/theater/:id", theaterHandler.showTheater)
	publicAPI.GET("/theater/:id/ticket", ticketHandler.findTicketByTheaterId)

	guardedAPI.GET("/order", orderHandler.IndexOrder)
	guardedAPI.POST("/ticket/:id/order", orderHandler.AddOrder)

	guardedAPI.POST("/password/change", authHandler.changePassword)
	guardedAPI.PUT("/self/change", authHandler.SelfUpdate)
	// TODO removed (this is a sample)
	guardedAPI.GET("/self", func(c *gin.Context) {
		user := GetAuthenticatedUser(c)
		ctx.Queue.AddJob(job.SendEmailJobName, user.Email)
		c.JSON(http.StatusOK, user)
	})
	// TODO removed (this is a sample) download my photo
	// guardedAPI.GET("/test_download_image", func(c *gin.Context) {
	// 	user := GetAuthenticatedUser(c)
	// 	photoFile, err := ctx.Storage.Read(user.Photo)
	// 	if err != nil {
	// 		c.JSON(http.StatusInternalServerError, errorResp(err.Error()))
	// 		return
	// 	}
	// 	defer photoFile.Close()

	// 	_, err = io.Copy(c.Writer, photoFile)
	// 	if err != nil {
	// 		c.JSON(http.StatusInternalServerError, errorResp(err.Error()))
	// 	}
	// })

	// TODO removed this sample for websocket
	guardedAPI.GET("/test_serve_ws", func(c *gin.Context) {
		user := GetAuthenticatedUser(c)
		messaging.WebSocketUpgrade(ctx.Messenger, user.Email, c.Writer, c.Request)
	})

	ctx.Messenger.Send(&messaging.OutgoingMessage{
		ReceiverID: "aris@gmail.com",
		SenderID:   "me@gmail.com",
		MessageID:  "random-uuid",
		Data:       "",
	})

}
