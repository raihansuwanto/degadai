package web

import (
	"fmt"
	"mime/multipart"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/satori/uuid"
	"github.com/sirupsen/logrus"
	"starter.co/api/app/model"
	"starter.co/api/app/provider"
	"starter.co/api/framework"
)

type ticketHandler struct {
	ctx *provider.Context
}

func (h *ticketHandler) IndexTicket(c *gin.Context) {

	// const layoutISO = "2006-01-02"
	// name := c.Query("name")
	// areaId, _ := strconv.ParseInt(c.Query("area_id"), 0, 64)
	// // if name != "" {
	var req struct {
		Name      string    `form:"name" json:"name"`
		AreaId    int64     `form:"area_id" json:"area_id"`
		DateStart time.Time `form:"date_start" json:"date_start" time_format:"2006-01-02"`
		DateEnd   time.Time `form:"date_end" json:"date_end" time_format:"2006-01-02"`
	}

	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error()))
		return
	}

	fmt.Println(req)

	data, err := h.ctx.TicketRepo.FindByQuery(req.Name, req.AreaId, req.DateStart, req.DateEnd)

	if err != nil {
		c.JSON(http.StatusBadRequest, errorResp("error add new customer"))
		return
	}

	c.JSON(http.StatusOK, successResp("retrieve customer data success", data))
	return
	// }

	// data, err := h.ctx.TicketRepo.Index()
	// if err != nil {
	// 	c.JSON(http.StatusBadRequest, errorResp("error add new ticket"))
	// 	return
	// }

	c.JSON(http.StatusOK, successResp("retrieve ticket data success", data))
}

func (h *ticketHandler) IndexPublic(c *gin.Context) {

	// const layoutISO = "2006-01-02"
	// name := c.Query("name")
	// areaId, _ := strconv.ParseInt(c.Query("area_id"), 0, 64)
	// // if name != "" {
	var req struct {
		Name        string `form:"name" json:"name"`
		AreaId      int64  `form:"area_id" json:"area_id"`
		NowPlaying  bool   `form:"now_playing" json:"now_playing"`
		CoomingSoon bool   `form:"cooming_soon" json:"cooming_soon"`
	}

	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error()))
		return
	}

	fmt.Println(req)

	data, err := h.ctx.TicketRepo.IndexQuery(req.Name, req.AreaId, req.NowPlaying, req.CoomingSoon)

	if err != nil {
		c.JSON(http.StatusBadRequest, errorResp("error add new customer"))
		return
	}

	c.JSON(http.StatusOK, successResp("retrieve customer data success", data))
}

func (h *ticketHandler) getTicketImages(c *gin.Context) {

	data, err := h.ctx.TicketRepo.Images()
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResp("error add new ticket"))
		return
	}

	c.JSON(http.StatusOK, successResp("retrieve ticket data success", data))
}

func (h *ticketHandler) AddTicket(c *gin.Context) {

	user := GetAuthenticatedUser(c)
	if !user.IsAdmin {
		c.JSON(http.StatusBadRequest, errorResp("You dont have access to make this request."))
		return
	}

	var req struct {
		Name        string                `form:"name" json:"name" binding:"required"`
		Description string                `form:"description" json:"description" binding:"required"`
		Stock       int64                 `form:"stock" json:"stock" binding:"required"`
		Image       *multipart.FileHeader `form:"image" json:"image"`
	}

	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error()))
		return
	}

	fmt.Println(req)

	path := ""

	if req.Image != nil {
		isImage, err := framework.IsMultipartImageFile(req.Image)
		if err != nil || !isImage {
			c.JSON(http.StatusBadRequest, errorResp("please upload correct image file"))
			return
		}

		file, err := req.Image.Open()
		if err != nil {
			c.JSON(http.StatusBadRequest, errorResp("error couldn't read upload file"))
			return
		}
		defer file.Close()

		imageFileName := fmt.Sprintf("img-%s", uuid.NewV4().String())
		path := fmt.Sprintf("ticket/images/%s", imageFileName)
		if err := h.ctx.Storage.Put(path, file, framework.VisibilityPublic); err != nil {
			logrus.Error(err)
			c.JSON(http.StatusUnprocessableEntity, errorResp("error couldn't store users photo"))
			return
		}
	}
	// logic process request

	ticket := &model.Ticket{
		Name:        req.Name,
		Description: req.Description,
		Stock:       req.Stock,
		Image:       path,
	}

	datErr := h.ctx.TicketRepo.Add(ticket)
	if datErr != nil {
		c.JSON(http.StatusBadRequest, errorResp("error add new ticket"))
		return
	}

	c.JSON(http.StatusOK, successResp("add new ticket success", nil))
}

func (h *ticketHandler) UpdateTicket(c *gin.Context) {

	user := GetAuthenticatedUser(c)
	if !user.IsAdmin {
		c.JSON(http.StatusBadRequest, errorResp("You dont have access to make this request."))
		return
	}

	// body, _ := ioutil.ReadAll(c.Request.Body)
	// println(string(body))

	id := c.Param("id")
	ticketID, _ := strconv.ParseInt(id, 0, 64)

	oldData, err := h.ctx.TicketRepo.FindByID(uint(ticketID))
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResp("error ticket Not Found"))
		return
	}

	var req struct {
		ID          int
		Name        string                `form:"name" json:"name"`
		Description string                `form:"description" json:"description"`
		Stock       int64                 `form:"stock" json:"stock"`
		Image       *multipart.FileHeader `form:"image" json:"image"`
	}

	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error()))
		return
	}
	fmt.Println(req.Image)

	// logic process request
	if req.Image != nil {
		isImage, err := framework.IsMultipartImageFile(req.Image)
		if err != nil || !isImage {
			c.JSON(http.StatusBadRequest, errorResp("please upload correct image file"))
			return
		}

		file, err := req.Image.Open()
		if err != nil {
			c.JSON(http.StatusBadRequest, errorResp("error couldn't read upload file"))
			return
		}
		defer file.Close()

		imageFileName := fmt.Sprintf("img-%s", uuid.NewV4().String())
		path := fmt.Sprintf("ticket/images/%s", imageFileName)
		if err := h.ctx.Storage.Put(path, file, framework.VisibilityPublic); err != nil {
			logrus.Error(err)
			c.JSON(http.StatusUnprocessableEntity, errorResp("error couldn't store users photo"))
			return
		}

		input := &model.Ticket{
			ID:          uint(ticketID),
			Name:        req.Name,
			Stock:       req.Stock,
			Description: req.Description,
			Image:       path,
		}

		data, err := h.ctx.TicketRepo.Update(input)
		if err != nil {
			c.JSON(http.StatusBadRequest, errorResp("error update ticket"))
			return
		}

		c.JSON(http.StatusOK, successResp("update ticket success", data))

	} else {
		input := &model.Ticket{
			ID:          uint(ticketID),
			Name:        req.Name,
			Stock:       req.Stock,
			Description: req.Description,
			Image:       oldData.Image,
		}

		data, err := h.ctx.TicketRepo.Update(input)
		if err != nil {
			c.JSON(http.StatusBadRequest, errorResp("error update ticket"))
			return
		}

		c.JSON(http.StatusOK, successResp("update ticket success", data))
	}
}

func (h *ticketHandler) UpdateTicketImage(c *gin.Context) {

	user := GetAuthenticatedUser(c)
	if !user.IsAdmin {
		c.JSON(http.StatusBadRequest, errorResp("You dont have access to make this request."))
		return
	}

	// body, _ := ioutil.ReadAll(c.Request.Body)
	// println(string(body))

	id := c.Param("id")
	ticketID, _ := strconv.ParseInt(id, 0, 64)

	// oldData, err := h.ctx.TicketRepo.FindByID(uint(ticketID))
	// if err != nil {
	// 	c.JSON(http.StatusBadRequest, errorResp("error ticket Not Found"))
	// 	return
	// }

	var req struct {
		ID    int
		Image *multipart.FileHeader `form:"image" json:"image"`
	}

	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error()))
		return
	}
	fmt.Println(req.Image)

	isImage, err := framework.IsMultipartImageFile(req.Image)
	if err != nil || !isImage {
		c.JSON(http.StatusBadRequest, errorResp("please upload correct image file"))
		return
	}

	file, err := req.Image.Open()
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResp("error couldn't read upload file"))
		return
	}
	defer file.Close()

	imageFileName := fmt.Sprintf("img-%s", uuid.NewV4().String())
	path := fmt.Sprintf("ticket/images/%s", imageFileName)
	if err := h.ctx.Storage.Put(path, file, framework.VisibilityPublic); err != nil {
		logrus.Error(err)
		c.JSON(http.StatusUnprocessableEntity, errorResp("error couldn't store users photo"))
		return
	}

	input := &model.Ticket{
		ID:    uint(ticketID),
		Image: path,
	}

	data, err := h.ctx.TicketRepo.Update(input)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResp("error update ticket"))
		return
	}

	c.JSON(http.StatusOK, successResp("update ticket success", data))
}

func (h *ticketHandler) FindTicket(c *gin.Context) {
	id := c.Param("id")
	ticketID, _ := strconv.ParseInt(id, 0, 64)
	data, err := h.ctx.TicketRepo.FindByID(uint(ticketID))
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResp("error ticket Not Found"))
		return
	}

	c.JSON(http.StatusOK, successResp("retrieve ticket data success", data))
}

func (h *ticketHandler) DeleteTicket(c *gin.Context) {
	id := c.Param("id")
	ticketID, _ := strconv.ParseInt(id, 0, 64)
	data, err := h.ctx.TicketRepo.DeleteByID(uint(ticketID))
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResp("error ticket Not Found"))
		return
	}

	c.JSON(http.StatusOK, successResp("retrieve ticket data success", data))
}

func (h *ticketHandler) findTicketByTheaterId(c *gin.Context) {
	id := c.Param("id")

	var req struct {
		Date time.Time `form:"date" json:"date" time_format:"2006-01-02"`
	}

	if err := c.ShouldBind(&req); err != nil {
		logrus.Error(err.Error())
		c.JSON(http.StatusBadRequest, errorResp(err.Error()))
		return
	}

	theaterID, _ := strconv.ParseInt(id, 0, 64)
	data, err := h.ctx.TicketRepo.FindMoviesByID(uint(theaterID), req.Date)
	if err != nil {
		c.JSON(http.StatusBadRequest, errorResp("error Theaters Not Found"))
		return
	}

	c.JSON(http.StatusOK, successResp("retrieve theater data success", data))
}

// func (h *ticketHandler) SearchTicketByName(c *gin.Context) {
// 	var req struct {
// 		ID          int
// 		Name        string `form:"name" json:"name"`
// 		Description string `form:"description" json:"description"`
// 		Stock       int64  `form:"stock" json:"stock"`
// 	}

// 	if err := c.ShouldBind(&req); err != nil {
// 		logrus.Error(err.Error())
// 		c.JSON(http.StatusBadRequest, errorResp(err.Error()))
// 		return
// 	}

// 	data, err := h.ctx.TicketRepo.FindByName(req.Name)
// 	if err != nil {
// 		c.JSON(http.StatusBadRequest, errorResp("error add new ticket"))
// 		return
// 	}

// 	c.JSON(http.StatusOK, successResp("retrieve ticket data success", data))
// }
