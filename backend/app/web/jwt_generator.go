package web

import (
	"fmt"
	"time"

	"starter.co/api/framework"

	"github.com/lestrrat-go/jwx/jwt"
)

// JWTGenerator a service use case for authenticating user
type JWTGenerator struct {
	accessTokenVerif  *framework.JWTVerifier
	refreshTokenVerif *framework.JWTVerifier
}

func (r *JWTGenerator) generateAccessToken(userID uint) (string, error) {
	jwtClaims := map[string]interface{}{
		jwt.IssuedAtKey:   time.Now().Unix(),
		jwt.ExpirationKey: time.Now().Add(time.Hour * 48).Unix(),
		"uid":             fmt.Sprintf("%d", userID),
	}
	return r.accessTokenVerif.GenerateJWT(jwtClaims)
}

func (r *JWTGenerator) generateRefreshToken(userID uint) (string, error) {
	refreshTokenClaims := map[string]interface{}{
		jwt.IssuedAtKey:   time.Now().Unix(),
		jwt.ExpirationKey: time.Now().Add(time.Hour * 24 * 30).Unix(),
		"uid":             fmt.Sprintf("%d", userID),
	}
	return r.refreshTokenVerif.GenerateJWT(refreshTokenClaims)
}

// GenerateNew returns (accessToken, refreshToken, error)
func (r *JWTGenerator) GenerateNew(userID uint) (string, string, error) {
	accessToken, err := r.generateAccessToken(userID)
	if err != nil {
		return "", "", err
	}

	refreshToken, err := r.generateRefreshToken(userID)
	return accessToken, refreshToken, err
}

// RefreshAccessToken returns (accessToken, refreshToken, error)
func (r *JWTGenerator) RefreshAccessToken(userID uint, refreshToken jwt.Token) (string, string, error) {
	accessToken, err := r.generateAccessToken(userID)
	if err != nil {
		return "", "", err
	}

	if refreshToken.Expiration().AddDate(0, 0, -5).Before(time.Now()) {
		refreshToken, err := r.generateRefreshToken(userID)
		return accessToken, refreshToken, err
	}
	return accessToken, "", nil
}
