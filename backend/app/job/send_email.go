package job

import (
	"bytes"
	"html/template"
	"io/ioutil"

	"github.com/markbates/pkger"
	"github.com/sirupsen/logrus"
	"gopkg.in/gomail.v2"
	"starter.co/api/app/provider"
	"starter.co/api/config"
	"starter.co/api/framework"
)

// SendEmailJobName identifier for send email job
const SendEmailJobName = "send_email"

type sendEmailHandler struct {
	ctx *provider.Context
	cfg *config.Config
}

// NewSendEmailHandler create job handler for sending email
func NewSendEmailHandler(ctx *provider.Context, cfg *config.Config) framework.JobHandler {
	return &sendEmailHandler{
		ctx: ctx,
		cfg: cfg,
	}
}

func (h *sendEmailHandler) Handle(jobName string, payload string) error {
	logrus.Infof("sending email job: %s\n", payload)
	receiver, err := h.ctx.UserRepo.FindByEmail(payload)
	if err != nil {
		return err
	}

	d := gomail.NewDialer(
		h.cfg.Email.Host,
		int(h.cfg.Email.Port),
		h.cfg.Email.UserName,
		h.cfg.Email.Password,
	)
	sender, err := d.Dial()
	if err != nil {
		panic(err)
	}
	defer sender.Close()

	file, err := pkger.Open("/template/mail_welcome.html")
	if err != nil {
		return err
	}
	fileData, err := ioutil.ReadAll(file)
	if err != nil {
		return err
	}

	tempOut := bytes.Buffer{}
	temp, _ := template.New("mail-welcome").Parse(string(fileData))
	temp.Execute(&tempOut, struct {
		Name string
	}{
		Name: receiver.Name,
	})

	email := gomail.NewMessage()
	email.SetHeader("From", "no-replay@videoshare.com")
	email.SetAddressHeader("To", receiver.Email, receiver.Name)
	email.SetHeader("Subject", "Welcome!")
	email.SetBody("text/html", tempOut.String())

	return gomail.Send(sender, email)
}
