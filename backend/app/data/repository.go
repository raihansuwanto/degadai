package data

import (
	"time"

	"starter.co/api/app/model"
)

// UserRepo user repository
type UserRepo interface {
	Index() (*[]model.User, error)
	Add(user *model.User) error
	Update(userID uint, input *model.User) error
	Remove(userID uint) error
	FindByID(userID uint) (*model.User, error)
	FindByEmail(email string) (*model.User, error)
	FindByName(name string) (*[]model.User, error)
	ChangePassword(userID uint, newPassword string) error
}

// CustomerRepo customer repository
type CustomerRepo interface {
	Index() (*[]model.Customer, error)
	Add(customer *model.Customer) error
	Update(input *model.Customer) error
	FindByName(name string) (*[]model.Customer, error)
	FindByID(customerID uint) (*[]model.Customer, error)
}

type TicketRepo interface {
	Index() (*[]ticketOrder, error)
	Images() (*[]model.Ticket, error)
	Add(ticket *model.Ticket) error
	Update(input *model.Ticket) (*model.Ticket, error)
	FindByQuery(name string, areaId int64, dateStart time.Time, dateEnd time.Time) (*[]ticketOrder, error)
	IndexQuery(name string, areaId int64, nowPlaying bool, coomingSoon bool) (*[]model.Ticket, error)
	FindByID(ticketID uint) (*ticketOrder, error)
	DeleteByID(ticketID uint) (*model.Ticket, error)
	FindMoviesByID(theaterID uint, date time.Time) (*[]model.Ticket, error)
}

type OrderRepo interface {
	Index() (*[]model.Order, error)
	Query(userID int, ticketID int) (*[]model.Order, error)
	Add(input *model.Order) error
	CountByTicketId(ticketID int64) int64
	CountByUserIdTicketId(ticketID int64, userID int64) int64
}

type AreaRepo interface {
	Index() (*[]model.Area, error)
}

type TheaterRepo interface {
	Index(areaId int64, name string) (*[]model.Theater, error)
	FindByID(theaterID uint) (*model.Theater, error)
	Add(theater *model.Theater) error
}

type TheaterTimeRepo interface {
}

type ShowRepo interface {
}
