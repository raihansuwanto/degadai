package data

import "gorm.io/gorm"

type transactionalMysql struct {
	db *gorm.DB
}

// NewTransactionalMysql create new transactional api for mysql repository
func NewTransactionalMysql(db *gorm.DB) Transactional {
	return &transactionalMysql{db: db}
}

func (t *transactionalMysql) CreateUnitOfWork() UnitOfWork {
	tx := t.db.Begin()
	return &unitOfWorkMysql{
		transaction: tx,
	}
}

type unitOfWorkMysql struct {
	transaction *gorm.DB
	userRepo    UserRepo
}

func (u *unitOfWorkMysql) GetUserRepo() UserRepo {
	if u.userRepo == nil {
		// cache the value so we can call GetUserRepo multiple times
		u.userRepo = &userRepoMysql{db: u.transaction}
	}
	return u.userRepo
}

func (u *unitOfWorkMysql) Complete() error {
	return u.transaction.Commit().Error
}

func (u *unitOfWorkMysql) Rollback() error {
	return u.transaction.Rollback().Error
}
