package data

import (
	"errors"
	"fmt"
	"time"

	"gorm.io/gorm"
	"starter.co/api/app/model"
)

type ticketRepoMysql struct {
	db *gorm.DB
}

var queryIndex string = "SELECT t.*,ke.ordered_stock  FROM tickets t LEFT JOIN (SELECT ticket_id,COUNT(*) AS ordered_stock FROM orders GROUP BY ticket_id) AS ke  ON ke.ticket_id = t.id WHERE t.deleted_at IS NULL"

// NewCustomerRepoMysql instantiate customer repository backed by mysql
func NewTicketRepoMysql(db *gorm.DB) TicketRepo {
	return &ticketRepoMysql{
		db: db,
	}
}

func (r *ticketRepoMysql) Add(ticket *model.Ticket) error {
	return r.db.Create(ticket).Error
}

func (r *ticketRepoMysql) Update(input *model.Ticket) (*model.Ticket, error) {

	result := r.db.Where("id = ?", input.ID).Updates(input)
	return input, result.Error
}

func (r *ticketRepoMysql) FindByQuery(name string, areaId int64, dateStart time.Time, dateEnd time.Time) (*[]ticketOrder, error) {
	var ticket []ticketOrder

	result := r.db
	if areaId == 0 {
		result = result.Raw(queryIndex+" AND name LIKE ?", "%"+name+"%", areaId).Find(&ticket)
	} else {
		result = result.Raw(queryIndex+" AND name LIKE ? AND area_id = ?", "%"+name+"%", areaId).Find(&ticket)
	}

	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, result.Error
	}

	return &ticket, nil
}

func (r *ticketRepoMysql) FindByID(ticketID uint) (*ticketOrder, error) {
	var ticket ticketOrder
	result := r.db.Raw(queryIndex+" AND t.id = ?", ticketID).First(&ticket)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, result.Error
	}

	return &ticket, nil
}

func (r *ticketRepoMysql) DeleteByID(ticketID uint) (*model.Ticket, error) {
	var ticket model.Ticket
	result := r.db.Where("id = ?", ticketID).Delete(&ticket)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, result.Error
	}

	return &ticket, nil
}

type ticketOrder struct {
	ID           uint
	Name         string `json:"name"`
	Image        string `json:"image"`
	Description  string `json:"description"`
	Stock        int64  `json:"stock"`
	AreaId       int64  `json:"area_id"`
	OrderedStock int64  `json:"ordered_stock"`
}

func (r *ticketRepoMysql) Index() (*[]ticketOrder, error) {
	var ticket []ticketOrder

	result := r.db.Raw(queryIndex).
		Find(&ticket)

	return &ticket, result.Error
}

func (r *ticketRepoMysql) IndexQuery(name string, areaId int64, nowPlaying bool, coomingSoon bool) (*[]model.Ticket, error) {
	var ticket []model.Ticket

	result := r.db

	if name != "" {
		result = result.Where("name LIKE ?", "%"+name+"%")
	}

	if areaId != 0 {
		result = result.Where("area_id = ?", areaId)
	}

	if nowPlaying {
		result = result.Where("date_start <= ?", time.Now()).Where("date_end >= ?", time.Now())
	}

	if coomingSoon {
		result = result.Where("date_start >= ?", time.Now())
	}

	final := result.Find(&ticket)

	return &ticket, final.Error
}

func (r *ticketRepoMysql) Images() (*[]model.Ticket, error) {
	var ticket []model.Ticket

	result := r.db.Where("image != ''").Find(&ticket)

	return &ticket, result.Error
}

func (r *ticketRepoMysql) FindMoviesByID(theaterID uint, date time.Time) (*[]model.Ticket, error) {
	var dest []model.Ticket
	var show []model.Shows

	filter := r.db.Select("ticket_id").Where("theater_id = ? AND date = ?", theaterID, date).Find(&show)

	result := r.db

	// .Where("id = ?", theaterID).Preload("Shows")

	fmt.Println(filter)

	if date.Year() != 0001 {
		result = result.Preload("Shows", "date = ?", date)
	}

	result = result.Where("id IN (?)", filter).
		// Preload("Shows", "theater_id = ? AND date = ?", theaterID, date).
		Preload("Shows.TheaterTime").
		Find(&dest)

	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, result.Error
	}

	return &dest, nil
}
