package data

import (
	"gorm.io/gorm"
)

type showRepoMysql struct {
	db *gorm.DB
}

func NewShowRepoMysql(db *gorm.DB) ShowRepo {
	return &showRepoMysql{
		db: db,
	}
}
