package data

import (
	"gorm.io/gorm"
	"starter.co/api/app/model"
)

type orderRepoMysql struct {
	db *gorm.DB
}

// NewUserRepoMysql instantiate user repository backed by mysql
func NewOrderRepoMysql(db *gorm.DB) OrderRepo {
	return &orderRepoMysql{
		db: db,
	}
}

func (r *orderRepoMysql) Index() (*[]model.Order, error) {
	var order []model.Order

	result := r.db.Joins("User").Joins("Ticket").Find(&order)

	return &order, result.Error
}

func (r *orderRepoMysql) Add(order *model.Order) error {
	return r.db.Create(order).Error
}

func (r *orderRepoMysql) Query(userID int, ticketID int) (*[]model.Order, error) {
	var order []model.Order

	result := r.db.Joins("User").Joins("Ticket").Find(&order)

	if userID != 0 {
		result.Where("user_id = ?", userID).Find(&order)
	}

	if ticketID != 0 {
		result.Where("ticket_id = ?", ticketID).Find(&order)
	}

	return &order, result.Error
}

func (r *orderRepoMysql) CountByTicketId(ticketID int64) int64 {
	var count int64
	var order *model.Order

	r.db.Model(&order).Where("ticket_id = ?", ticketID).Count(&count)

	return count
}

func (r *orderRepoMysql) CountByUserIdTicketId(ticketID int64, userID int64) int64 {
	var count int64
	var order *model.Order

	r.db.Model(&order).Where("user_id = ?", userID).Where("ticket_id = ?", ticketID).Count(&count)

	return count
}
