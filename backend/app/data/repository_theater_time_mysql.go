package data

import (
	"gorm.io/gorm"
)

type theaterTimeRepoMysql struct {
	db *gorm.DB
}

func NewTheaterTimeRepoMysql(db *gorm.DB) TheaterTimeRepo {
	return &theaterTimeRepoMysql{
		db: db,
	}
}
