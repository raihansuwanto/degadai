package data

import (
	"errors"

	"gorm.io/gorm"
	"starter.co/api/app/model"
)

type theaterRepoMysql struct {
	db *gorm.DB
}

func NewTheaterRepoMysql(db *gorm.DB) TheaterRepo {
	return &theaterRepoMysql{
		db: db,
	}
}

func (r *theaterRepoMysql) Add(theater *model.Theater) error {
	return r.db.Create(theater).Error
}

func (r *theaterRepoMysql) Index(areaId int64, name string) (*[]model.Theater, error) {

	var theater []model.Theater

	result := r.db

	if areaId != 0 {
		result = result.Where("area_id = ?", areaId)
	}

	if name != "" {
		result = result.Where("name LIKE ?", "%"+name+"%")
	}

	result = result.Preload("Area").Find(&theater)

	return &theater, result.Error
}

func (r *theaterRepoMysql) FindByID(theaterID uint) (*model.Theater, error) {
	var dest model.Theater
	result := r.db.Where("id = ?", theaterID)

	// .Preload("Shows")

	// result = result.Preload("Shows", "date >= ?", time.Now())

	// result = result.Preload("Shows", "date <= ?", time.Now())

	result = result.First(&dest)

	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, result.Error
	}

	return &dest, nil
}
