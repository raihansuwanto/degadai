package data

import (
	"errors"

	"gorm.io/gorm"
	"starter.co/api/app/model"
)

type userRepoMysql struct {
	db *gorm.DB
}

// NewUserRepoMysql instantiate user repository backed by mysql
func NewUserRepoMysql(db *gorm.DB) UserRepo {
	return &userRepoMysql{
		db: db,
	}
}

func (r *userRepoMysql) Index() (*[]model.User, error) {
	var user []model.User

	result := r.db.Find(&user)

	return &user, result.Error
}

func (r *userRepoMysql) Add(user *model.User) error {
	return r.db.Create(user).Error
}

func (r *userRepoMysql) Update(userID uint, input *model.User) error {
	return r.db.
		Where("id = ?", userID).
		Updates(input).
		Error
}

func (r *userRepoMysql) Remove(userID uint) error {
	return r.db.Model(&model.User{}).Delete("id = ?", userID).Error
}

func (r *userRepoMysql) FindByID(userID uint) (*model.User, error) {
	var dest model.User
	result := r.db.Model(&model.User{}).Where("id = ?", userID).First(&dest)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, result.Error
	}

	return &dest, nil
}

func (r *userRepoMysql) FindByEmail(email string) (*model.User, error) {
	var dest model.User
	result := r.db.Model(&model.User{}).Where("email = ?", email).First(&dest)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, result.Error
	}

	return &dest, nil
}

func (r *userRepoMysql) FindByName(name string) (*[]model.User, error) {
	var dest []model.User
	result := r.db.Where("name LIKE ?", "%"+name+"%").Find(&dest)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, result.Error
	}

	return &dest, nil
}

func (r *userRepoMysql) ChangePassword(userID uint, newPassword string) error {
	return r.db.Model(&model.User{}).Where("id = ?", userID).Update("password", newPassword).Error
}
