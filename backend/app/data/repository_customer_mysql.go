package data

import (
	"errors"

	"gorm.io/gorm"
	"starter.co/api/app/model"
)

type customerRepoMysql struct {
	db *gorm.DB
}

// NewCustomerRepoMysql instantiate customer repository backed by mysql
func NewCustomerRepoMysql(db *gorm.DB) CustomerRepo {
	return &customerRepoMysql{
		db: db,
	}
}

func (r *customerRepoMysql) Add(customer *model.Customer) error {
	return r.db.Create(customer).Error
}

func (r *customerRepoMysql) Update(input *model.Customer) error {
	return r.db.
		Where("id = ?", input.ID).
		Updates(input).
		Error
}

func (r *customerRepoMysql) FindByName(name string) (*[]model.Customer, error) {
	var dest []model.Customer
	result := r.db.Where("name LIKE ?", "%"+name+"%").Find(&dest)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, result.Error
	}

	return &dest, nil
}

func (r *customerRepoMysql) FindByID(customerID uint) (*[]model.Customer, error) {
	var dest []model.Customer
	result := r.db.Where("id = ?", customerID).First(&dest)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		}
		return nil, result.Error
	}

	return &dest, nil
}

func (r *customerRepoMysql) Index() (*[]model.Customer, error) {
	var cust []model.Customer

	result := r.db.Find(&cust)

	return &cust, result.Error
}
