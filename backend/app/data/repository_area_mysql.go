package data

import (
	"gorm.io/gorm"
	"starter.co/api/app/model"
)

type areaRepoMysql struct {
	db *gorm.DB
}

func NewAreaRepoMysql(db *gorm.DB) AreaRepo {
	return &areaRepoMysql{
		db: db,
	}
}

func (r *areaRepoMysql) Index() (*[]model.Area, error) {
	var area []model.Area

	result := r.db.Find(&area)

	return &area, result.Error
}
