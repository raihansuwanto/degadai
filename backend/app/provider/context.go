package provider

import (
	"starter.co/api/app/data"
	"starter.co/api/framework"
	"starter.co/api/framework/messaging"
)

// Context store global resources access in application
type Context struct {
	Cache                framework.Cache
	Event                framework.Event
	Queue                framework.Queue
	Storage              framework.Storage
	AccessTokenVerifier  *framework.JWTVerifier
	RefreshTokenVerifier *framework.JWTVerifier
	Messenger            messaging.Messenger
	UserRepo             data.UserRepo
	CustomerRepo         data.CustomerRepo
	TicketRepo           data.TicketRepo
	OrderRepo            data.OrderRepo
	AreaRepo             data.AreaRepo
	ShowRepo             data.ShowRepo
	TheaterRepo          data.TheaterRepo
	TheaterTimeRepo      data.TheaterTimeRepo
	Transactional        data.Transactional
}
