ALTER TABLE `orders`
  ADD CONSTRAINT `fk_order_ticket` FOREIGN KEY (`ticket_id`) REFERENCES `tickets`(`id`),
  ADD CONSTRAINT `fk_order_user` FOREIGN KEY (`user_id`) REFERENCES `users`(`id`);