CREATE TABLE IF NOT EXISTS shows (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    area_id BIGINT NULL,
    theater_id BIGINT NULL,
    theater_times_id BIGINT NULL,
    ticket_id BIGINT NULL,
    date DATE NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP NULL DEFAULT NULL,
    INDEX(deleted_at)
) ENGINE=INNODB;