CREATE TABLE IF NOT EXISTS users (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    serial VARCHAR(255) UNIQUE NOT NULL,
    name VARCHAR(255),
    password VARCHAR(255),
    is_admin TINYINT DEFAULT 0,
    email VARCHAR(255) UNIQUE NOT NULL,
    address VARCHAR(255),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP NULL DEFAULT NULL,
    INDEX(deleted_at),
    INDEX(email)
) ENGINE=INNODB;

insert  into `users`(`id`,`serial`,`name`,`password`,`is_admin`,`email`) values 
(1,'usr-895b5a09-2772-4e11-9577-c8a5c0258959','admin','$2a$14$Z1rFv7/lahmIkbsfFjWrHejQm1xV/dP0xFAQuApSvwsjLlA.ZajDS',1,'admin@degadai.id');