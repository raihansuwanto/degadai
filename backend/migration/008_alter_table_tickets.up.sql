ALTER TABLE `tickets`
  ADD COLUMN `date_start` DATE NULL AFTER `stock`,
	ADD COLUMN `date_end` DATE NULL AFTER `date_start`;
