## framework package 📦

Contains general utilities and tools commonly used in web application. You can reuse this package easily for different projects since it's an independent package.

### Cache

Provides general purpose simple key value pair caching mechanism implmented using redis.

Usage:
```go
cache := framework.NewCacheRedis(redisClient, "starter-app")

// put in cache forever
cache.Put("key0", "you data payload here")

// this will disappear in 1 minute
cache.PutWithTTL("key1", "your data payload here", time.Minute)

// 
value, err := cache.Get("key1")
```

### Event

Event is a pub/sub mechanism, provided implementation using redis pub/sub.
> note that redis pub/sub is broadcast

### JWT

Provide functionality to generate JWS/JWE token and verify them with given private key

### Queue

Queue provide common job queuing functionality for asyncronous execution.
Implemented using database `queue_db.go` or using redis `queue_redis.go`

Usage:
```go
type sendEmailHandler struct {}

func (s sendEmailHandler) Handle(jobName string, payload string) error {
    // Send email logic here
}

func main() {
    queue := framework.NewQueueDB(gormDB, 5)

    // first add job handler that will be called if there is a job needs to be run
    queue.AddJobHandler("send_email", &sendEmailHandler{})

    // queue the job
    queue.AddJob("send_email", "aris@gmail.com")

    // execute job after 30 secs
    queue.AddDelayedJob("send_email", "aris@gmail.com", 30)

    // stop queue
    queue.Stop()
}
```

### Schedule

Implement periodic job scheduler, you provide cron spec as it's scheduling pattern. this implementation is safe to run on multiple instance, but at the same time only one job for a particular schedule will be run.

> note that this is implemented using redis, all scheduler instance will schedule and when there is overdue job it will acquire a lock for that job and only one instance would win

### Storage

Provide storage abstraction for working with files/persistance object.

Storage implementation:
- `AWS S3`
- `Alibaba OSS`
- `Local File Storage`

Usage:

```go
// first param is where the private file will be stored
// second param is where the public hosted file will be stored (note that this directory needs to be served by your web server to give access)
// third param is the base url for this hosted file, it is use to generate URL from object key (path) and append this base url to build full url
storage := framework.NewStorageLocalFile("./storage/private", "./storage/public", "http://localhost/files")

stream, err := storage.Read("users/images/profile.jpg")
```

### Other Utilities

**Password Hash**

bcrypt password hash function

Usage:
```go
hashed, err := framework.HashPassword("your_password")
if err != nil {
    panic(err)
}

valid := framework.CheckPasswordHash("your_password", hashed)
if valid {
    fmt.Println("your password correct")
}
```
