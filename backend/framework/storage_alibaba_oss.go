package framework

import (
	"fmt"
	"io"
	"net/http"
	"path"
	"path/filepath"
	"strconv"
	"time"

	"github.com/aliyun/aliyun-oss-go-sdk/oss"
)

const signedURLExpiration = 30 * 60 // 30 minutes

type storageAlibabaOSS struct {
	client *oss.Client
	bucket *oss.Bucket
}

// NewStorageAlibabaOSS create storage backed by alibaba oss
func NewStorageAlibabaOSS(
	bucketName string,
	endpoint string,
	accessID string,
	accessSecret string) Storage {

	client, err := oss.New(endpoint, accessID, accessSecret)
	if err != nil {
		panic(err)
	}

	bucket, err := client.Bucket(bucketName)
	if err != nil {
		panic(err)
	}

	return &storageAlibabaOSS{
		client: client,
		bucket: bucket,
	}
}

func cleanOSSObjectPath(objectPath string) string {
	return path.Clean(filepath.ToSlash(objectPath))
}

func (s *storageAlibabaOSS) Read(objectPath string) (io.ReadCloser, error) {
	return s.bucket.GetObject(cleanOSSObjectPath(objectPath))
}

func (s *storageAlibabaOSS) Put(objectPath string, source io.Reader, options ...Option) error {
	var ossOptions []oss.Option
	for _, option := range options {
		if option == VisibilityPublic {
			ossOptions = append(ossOptions, oss.ObjectACL(oss.ACLPublicRead))
		}
	}
	return s.bucket.PutObject(cleanOSSObjectPath(objectPath), source, ossOptions...)
}

func (s *storageAlibabaOSS) Delete(objectPath string) error {
	return s.bucket.DeleteObject(cleanOSSObjectPath(objectPath))
}

func (s *storageAlibabaOSS) Copy(srcObjectPath string, dstObjectPath string) error {
	_, err := s.bucket.CopyObject(cleanOSSObjectPath(srcObjectPath), cleanOSSObjectPath(dstObjectPath))
	return err
}

func (s *storageAlibabaOSS) URL(objectPath string, isPublicObject bool) (string, error) {
	objectPath = cleanOSSObjectPath(objectPath)
	if isPublicObject {
		return fmt.Sprintf("https://%s.%s/%s", s.bucket.BucketName, s.bucket.GetConfig().Endpoint, objectPath), nil
	}
	return s.bucket.SignURL(objectPath, oss.HTTPGet, signedURLExpiration)
}

func (s *storageAlibabaOSS) Size(objectPath string) (int64, error) {
	r, err := s.bucket.GetObjectMeta(cleanOSSObjectPath(objectPath))
	if err != nil {
		return 0, err
	}

	sizeStr := r.Get("Content-Length")
	return strconv.ParseInt(sizeStr, 10, 64)
}

func (s *storageAlibabaOSS) LastModified(objectPath string) (time.Time, error) {
	r, err := s.bucket.GetObjectMeta(cleanOSSObjectPath(objectPath))
	if err != nil {
		return time.Time{}, err
	}

	LastModified, err := http.ParseTime(r.Get("Last-Modified"))
	if err != nil {
		return time.Time{}, err
	}

	return LastModified, nil
}
