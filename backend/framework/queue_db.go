package framework

import (
	"fmt"
	"math/rand"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

const (
	statusWaiting    = "waiting"
	statusProcessing = "processing"
	statusComplete   = "complete"
)

type job struct {
	gorm.Model
	JobName     string
	Payload     string
	Status      string `gorm:"index"`
	RunAt       time.Time
	LastVisited time.Time `gorm:"index"`
}

type queueDB struct {
	startMutex  sync.Mutex
	running     bool
	workerCount int
	db          *gorm.DB
	handlers    map[string]JobHandler
	mutex       sync.RWMutex
}

// NewQueueDB create job queue backend by database
func NewQueueDB(db *gorm.DB, workerCount int) (Queue, error) {
	if workerCount <= 0 {
		panic(fmt.Errorf("Queue job worker count must not be <= 0"))
	}

	err := db.AutoMigrate(&job{})
	if err != nil {
		return nil, err
	}

	return &queueDB{
		db:          db,
		handlers:    make(map[string]JobHandler),
		startMutex:  sync.Mutex{},
		mutex:       sync.RWMutex{},
		workerCount: workerCount,
		running:     false,
	}, nil
}

func (q *queueDB) AddJob(jobName string, payload string) error {
	return q.AddDelayedJob(jobName, payload, 0)
}

func (q *queueDB) AddDelayedJob(jobName string, payload string, delaySecs uint) error {
	runAt := time.Now()
	if delaySecs > 0 {
		runAt = time.Now().Add(time.Second * time.Duration(delaySecs))
	}
	j := job{
		JobName:     jobName,
		Payload:     payload,
		Status:      statusWaiting,
		RunAt:       runAt,
		LastVisited: time.Now(),
	}
	return q.db.Create(&j).Error
}

func (q *queueDB) AddJobHandler(jobName string, handler JobHandler) {
	q.mutex.Lock()
	defer q.mutex.Unlock()
	q.handlers[jobName] = handler
}

func (q *queueDB) Start() {
	q.startMutex.Lock()
	defer q.startMutex.Unlock()
	if !q.running {
		q.running = true
		for i := 0; i < q.workerCount; i++ {
			go q.startWorkerLoop()
		}
		go q.startJobRequeueLoop()
		logrus.Info("[QueueDB] worker and scheduler running...")
	}
}

func (q *queueDB) Close() {
	// TODO stops worker and requeuer
}

func (q *queueDB) startJobRequeueLoop() {
	for {
		var res job
		tx := q.db.Begin()
		err := tx.
			Clauses(clause.Locking{Strength: "UPDATE"}).
			Where("status = ? AND last_visited <= ?", statusProcessing, time.Now().Add(-maxLastVisitedDuration)).
			First(&res).Error
		if err != nil {
			tx.Rollback()
			if err == gorm.ErrRecordNotFound {
				time.Sleep(time.Minute * 10)
			} else {
				time.Sleep(time.Second * 10)
			}
			continue
		}

		err = tx.Model(&job{}).
			Where("id = ?", res.ID).
			Updates(job{Status: statusWaiting, LastVisited: time.Now()}).Error
		if err != nil {
			tx.Rollback()
			continue
		}
		tx.Commit()

		logrus.Debugf("[qdb] requeue job %s - %d\n", res.JobName, res.ID)
	}
}

func (q *queueDB) startWorkerLoop() {
	for {
		j, err := q.findJobToProcess()
		if err != nil {
			time.Sleep(time.Second * 5)
			continue
		}

		if handler, ok := q.handlers[j.JobName]; ok {
			visitor := jobVisitor{stopChannel: make(chan bool)}
			go visitor.startVisiting(func() {
				_ = q.updateLastVisited(j.ID)
			})
			err := handler.Handle(j.JobName, j.Payload)
			visitor.stop()

			if err == nil {
				_ = q.updateJobStatus(j.ID, statusComplete)
			} else {
				_ = q.updateJobStatus(j.ID, statusWaiting)
			}
		} else {
			_ = q.updateJobStatus(j.ID, statusWaiting)
		}

		time.Sleep(time.Second * time.Duration(rand.Intn(10)))
	}
}

func (q *queueDB) findJobToProcess() (*job, error) {
	var res job
	tx := q.db.Begin()
	err := tx.
		Clauses(clause.Locking{Strength: "UPDATE"}).
		Where("status = ? AND (run_at <= ? OR run_at IS NULL)", statusWaiting, time.Now()).
		Limit(1).
		First(&res).Error
	if err != nil {
		tx.Rollback()
		return nil, err
	}

	err = tx.Model(&job{}).
		Where("id = ?", res.ID).
		Updates(job{Status: statusProcessing, LastVisited: time.Now()}).Error
	if err != nil {
		tx.Rollback()
		return nil, err
	}
	tx.Commit()
	return &res, nil
}

func (q *queueDB) updateJobStatus(jobID uint, status string) error {
	return q.db.Transaction(func(tx *gorm.DB) error {
		return tx.Model(&job{}).
			Where("id = ?", jobID).
			Update("status", status).Error
	})
}

func (q *queueDB) updateLastVisited(jobID uint) error {
	return q.db.Transaction(func(tx *gorm.DB) error {
		return tx.Model(&job{}).
			Where("id = ?", jobID).
			Update("last_visited", time.Now()).Error
	})
}

// jobVisitor act as heartbeat to inform that this job is still processing
type jobVisitor struct {
	stopChannel chan bool
}

func (v *jobVisitor) stop() {
	v.stopChannel <- true
}

func (v *jobVisitor) startVisiting(onVisiting func()) {
	for {
		ticker := time.NewTicker(visitingInterval)
		select {
		case <-v.stopChannel:
			ticker.Stop()
			return
		case <-ticker.C:
			onVisiting()
			break
		}
	}
}

const (
	visitingInterval       = time.Second * 10
	maxLastVisitedDuration = time.Minute * 15
)
