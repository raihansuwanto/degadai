package messaging

import (
	"github.com/gorilla/websocket"
	"sync"
)

type (
	connectionInfo struct {
		clientID string
		serverID string
	}

	globalConnectionInfo interface {
		Get(sessionID string) (connectionInfo, error)
		Put(info connectionInfo) error
		Remove(userSerial string) error
		RemoveAll(connections *localConnections) error
	}

	connection struct {
		socket   *websocket.Conn
		clientID string
		writeMtx sync.Mutex
	}

	localConnections struct {
		mutex       sync.RWMutex
		connections map[string]*connection
	}
)

func newLocalConnectionStore() localConnections {
	return localConnections{
		connections: make(map[string]*connection),
	}
}

func (l *localConnections) Put(key string, client *connection) {
	l.mutex.Lock()
	defer l.mutex.Unlock()
	l.connections[key] = client
}

func (l *localConnections) Get(key string) *connection {
	l.mutex.RLock()
	defer l.mutex.RUnlock()
	return l.connections[key]
}

func (l *localConnections) Remove(key string) {
	l.mutex.Lock()
	defer l.mutex.Unlock()
	delete(l.connections, key)
}

func (l *localConnections) IterateAndPop(callback func(key string, value *connection)) {
	l.mutex.Lock()
	defer l.mutex.Unlock()
	for clientID, conn := range l.connections {
		delete(l.connections, clientID)
		callback(clientID, conn)
	}
}

func (l *localConnections) Iterate(callback func(key string, value *connection)) {
	l.mutex.RLock()
	defer l.mutex.RUnlock()
	for k, v := range l.connections {
		callback(k, v)
	}
}