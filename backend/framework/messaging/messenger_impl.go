package messaging

import (
	"encoding/json"
	"sync"

	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
	"starter.co/api/framework"

	"github.com/gorilla/websocket"
)

type messengerImpl struct {
	serverID             string
	localConnections     localConnections
	globalConnectionInfo globalConnectionInfo
	event                framework.Event
}

// NewMessenger create new instance for Messenger implementation
func NewMessenger(serverID string, event framework.Event, redisClient *redis.Client) Messenger {
	messenger := &messengerImpl{
		serverID:             serverID,
		localConnections:     newLocalConnectionStore(),
		globalConnectionInfo: newGlobalConnectionInfo(redisClient, serverID),
		event:                event,
	}
	_ = event.Subscribe(messenger.serverID, messenger)
	return messenger
}

func (m *messengerImpl) Send(message *OutgoingMessage) {
	m.sendMessageImpl(message)
}

func (m *messengerImpl) TearDown() {
	logrus.Infof("Tearing down messenger client connections, conn num: %d", len(m.localConnections.connections))
	_ = m.globalConnectionInfo.RemoveAll(&m.localConnections)
	// TODO re-check if this is called after servers graceful shutdown the it's not necessary
	m.localConnections.IterateAndPop(func(key string, value *connection) {
		_ = value.socket.Close()
	})
}

func (m *messengerImpl) AddClient(clientID string, socket *websocket.Conn) error {
	clientConn := connection{
		socket:   socket,
		clientID: clientID,
		writeMtx: sync.Mutex{},
	}

	clientInfo := connectionInfo{
		clientID: clientConn.clientID,
		serverID: m.serverID,
	}

	if err := m.globalConnectionInfo.Put(clientInfo); err != nil {
		return err
	} else {
		m.localConnections.Put(clientConn.clientID, &clientConn)
		go m.startClientReadLoop(&clientConn)
	}
	return nil
}

func (m *messengerImpl) Handle(_ string, payload string) {
	var message OutgoingMessage
	if err := json.Unmarshal([]byte(payload), &message); err != nil {
		return
	}

	receiver := m.localConnections.Get(message.ReceiverID)
	if receiver != nil {
		// TODO maybe run on different go routine
		m.pushMessage(receiver, &message)
	}
}

func (m *messengerImpl) startClientReadLoop(client *connection) {
	for {
		_, msg, err := client.socket.ReadMessage()
		if err != nil {
			m.removeClient(client.clientID)
			logrus.Debugf("error while reading websocket message: %s\n", err.Error())
			break
		}

		var message IncomingMessage
		err = json.Unmarshal(msg, &message)
		if err != nil || message.IsEmpty() {
			logrus.Debugf("message parsing error from %s", client.clientID)
			continue
		}

		if validateIncomingMessage(&message) {
			m.processIncomingMessage(client, &message)
		}
	}
}

func (m *messengerImpl) processIncomingMessage(sender *connection, message *IncomingMessage) {
	// TODO maybe run on new go routine

	outgoingMsg := &OutgoingMessage{
		MessageID:  message.MessageID,
		SenderID:   sender.clientID,
		ReceiverID: message.ReceiverID,
		Data:       message.Data,
	}

	m.sendMessageImpl(outgoingMsg)
}

func (m *messengerImpl) sendMessageImpl(message *OutgoingMessage) {
	if !validateOutgoingMessage(message) {
		return
	}

	receiver := m.localConnections.Get(message.ReceiverID)
	if receiver != nil {
		// Receiver is online on this server
		m.pushMessage(receiver, message)
	} else {
		clientInfo, err := m.globalConnectionInfo.Get(message.ReceiverID)
		if err == nil {
			if messageData, err := json.Marshal(message); err == nil {
				// Receiver is online on another server
				_ = m.event.Publish(clientInfo.serverID, string(messageData))
			}
		}
		// If goes here then receiver is offline
	}
}

func (m *messengerImpl) removeClient(userSerial string) {
	_ = m.globalConnectionInfo.Remove(userSerial)
	client := m.localConnections.Get(userSerial)
	if client != nil {
		m.localConnections.Remove(userSerial)
		_ = client.socket.Close()
	}
}

func (m *messengerImpl) pushMessage(receiver *connection, message *OutgoingMessage) {
	data, err := json.Marshal(message)
	if err != nil {
		return
	}

	receiver.writeMtx.Lock()
	_ = receiver.socket.WriteMessage(websocket.TextMessage, data)
	receiver.writeMtx.Unlock()
}

func validateOutgoingMessage(message *OutgoingMessage) bool {
	return message.MessageID != "" &&
		message.SenderID != "" &&
		message.ReceiverID != ""
}

func validateIncomingMessage(message *IncomingMessage) bool {
	return message.MessageID != "" &&
		message.ReceiverID != ""
}
