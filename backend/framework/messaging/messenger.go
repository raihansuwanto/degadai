package messaging

// Messenger interface for real time data connection between users and servers
type Messenger interface {
	Send(message *OutgoingMessage)
	TearDown()
}

// IncomingMessage message coming from client to server
type IncomingMessage = OutgoingMessage

// OutgoingMessage message out from server to client
type OutgoingMessage struct {
	MessageID  string      `json:"id"`
	SenderID   string      `json:"sender"`
	ReceiverID string      `json:"receiver"`
	Data       interface{} `json:"data"`
}

// IsEmpty return true if all fields empty
func (m *OutgoingMessage) IsEmpty() bool {
	return m.MessageID == "" &&
		m.SenderID == "" &&
		m.ReceiverID == "" &&
		m.Data == nil
}
