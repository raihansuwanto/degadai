package framework

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"io/ioutil"
	"os"

	"github.com/lestrrat-go/jwx/jwa"
	"github.com/lestrrat-go/jwx/jwe"
	"github.com/lestrrat-go/jwx/jwt"
)

// JWTVerifier generate and verify JWT
type JWTVerifier struct {
	privateKey *rsa.PrivateKey
}

// NewJWTVerifier create new Jwt verification
func NewJWTVerifier(privateKeyPath string) (*JWTVerifier, error) {
	file, err := os.Open(privateKeyPath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	data, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	if privateKey, err := getRSAPrivateKey(data); err != nil {
		return nil, err
	} else {
		return &JWTVerifier{
			privateKey: privateKey,
		}, nil
	}
}

// NewJWTVerifierWithPEM create new jwt verification with provided private key
func NewJWTVerifierWithPEM(pem string) (*JWTVerifier, error) {
	if privateKey, err := getRSAPrivateKey([]byte(pem)); err != nil {
		return nil, err
	} else {
		return &JWTVerifier{
			privateKey: privateKey,
		}, nil
	}
}

// JWTClaims store all jwt claims
type JWTClaims map[string]interface{}

func getRSAPrivateKey(pemPrivateKey []byte) (*rsa.PrivateKey, error) {
	block, _ := pem.Decode(pemPrivateKey)
	if block == nil {
		return nil, errors.New("no PEM data found in the input bytes")
	}
	return x509.ParsePKCS1PrivateKey(block.Bytes)
}

// GenerateJWT create new jwe message (sign and encrypt)
func (j *JWTVerifier) GenerateJWT(claims JWTClaims) (string, error) {
	t := jwt.New()
	for k, v := range claims {
		t.Set(k, v)
	}

	signed, err := jwt.Sign(t, jwa.RS256, j.privateKey)
	if err != nil {
		return "", err
	}

	encrypted, err := jwe.Encrypt([]byte(signed), jwa.RSA1_5, &j.privateKey.PublicKey, jwa.A128CBC_HS256, jwa.NoCompress)
	if err != nil {
		return "", err
	}

	return string(encrypted), nil
}

// VerifyJWT decrypt and verify string jwe message in token
func (j *JWTVerifier) VerifyJWT(token string) (jwt.Token, error) {
	decrypted, err := jwe.Decrypt([]byte(token), jwa.RSA1_5, j.privateKey)
	if err != nil {
		return nil, err
	}

	t, err := jwt.ParseBytes(decrypted, jwt.WithVerify(jwa.RS256, j.privateKey.PublicKey))
	if err != nil {
		return nil, err
	}

	err = jwt.Validate(t)
	if err != nil {
		return nil, err
	}

	return t, nil
}
