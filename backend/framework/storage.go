package framework

import (
	"io"
	"time"
)

// Option for storage implementation
type Option string

const (
	// VisibilityPublic option suggest implementation to set object as public read
	VisibilityPublic Option = "public"
)

// Storage is an abstraction for persitence storage mechanism,
// remember that all object path used here should be specified
// relative to the root location configured for each implementation
type Storage interface {
	// Read return reader to stream data from source
	Read(objectPath string) (io.ReadCloser, error)

	// Put store source stream into
	Put(objectPath string, source io.Reader, options ...Option) error

	// Delete object by objectPath
	Delete(objectPath string) error

	// URL return object url
	URL(objectPath string, isPublicObject bool) (string, error)

	// Copy source to destination
	Copy(srcObjectPath string, dstObjectPath string) error

	// Size return object size
	Size(objectPath string) (int64, error)

	// LastModified return last modified time of object
	LastModified(objectPath string) (time.Time, error)

	// TODO implement Move(srcObjectPath string, dstObjectPath string) error
	// TODO implement SetVisibility(objectPath string, Visibility visibility) error
	// TODO implement GetVisibility(objectPath string) (Visibility, error)
	// TODO implement Append(objectPath string, data []byte) error
	// TODO implement Prepend(objectPath string, data[]byte) error
}
