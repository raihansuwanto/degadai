package cmd

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"starter.co/api/framework"
	"starter.co/api/framework/messaging"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"github.com/satori/uuid"
	"github.com/sirupsen/logrus"
	"starter.co/api/app/data"
	"starter.co/api/app/provider"
	"starter.co/api/app/web"
	"starter.co/api/config"
)

// initialize all background queue job handlers
func initJobHandlers(ctx *provider.Context, cfg *config.Config, queue framework.Queue) {
	// queue.AddJobHandler(job.SendEmailJobName, job.NewSendEmailHandler(ctx, cfg))
	// queue.Start()
}

func createStorage(cfg *config.Config, storageType string) framework.Storage {
	switch {
	case storageType == "oss":
		return framework.NewStorageAlibabaOSS(
			cfg.AlibabaOSS.BucketName,
			cfg.AlibabaOSS.Endpoint,
			cfg.AlibabaOSS.AccessID,
			cfg.AlibabaOSS.AccessSecret)
	case storageType == "s3":
		return framework.NewStorageAWSS3(
			cfg.S3.BucketName,
			cfg.AWS.Region,
			cfg.AWS.AccessKeyID,
			cfg.AWS.SecretAccessKey,
			cfg.AWS.SessionToken)
	case storageType == "local":
		return framework.NewStorageLocalFile(
			cfg.LocalStorage.BaseDir,
			cfg.LocalStorage.PublicBaseDir,
			cfg.LocalStorage.BaseURL)
	}
	return nil
}

func serve() {
	cfg := getConfig()
	if cfg.Mode == config.ModeProd {
		logrus.SetLevel(logrus.InfoLevel)
		logrus.SetFormatter(&logrus.JSONFormatter{})
	} else {
		logrus.SetLevel(logrus.DebugLevel)
		logrus.SetFormatter(&logrus.TextFormatter{ForceColors: true, DisableColors: false})
		logrus.Debug(cfg)
	}

	db := createDatabaseConnection(cfg)
	redisCli := createRedisClient(cfg)

	storage := createStorage(cfg, "local")
	accessTokenVerifier, refreshTokenVerifier := getAccessAndRefreshTokenVerifier(cfg)
	cache := framework.NewCacheRedis(redisCli, cfg.AppName)
	event := framework.NewEventRedis(redisCli)
	queue := framework.NewQueueRedis(cfg.AppName, cfg.Redis.Address, cfg.Redis.Password)
	defer queue.Close()

	userRepo := data.NewUserRepoMysql(db)
	customerRepo := data.NewCustomerRepoMysql(db)
	ticketRepo := data.NewTicketRepoMysql(db)
	orderRepo := data.NewOrderRepoMysql(db)
	areaRepo := data.NewAreaRepoMysql(db)
	showRepo := data.NewShowRepoMysql(db)
	theaterTimeRepo := data.NewTheaterTimeRepoMysql(db)
	theaterRepo := data.NewTheaterRepoMysql(db)
	transactional := data.NewTransactionalMysql(db)

	serverID := uuid.NewV4().String()
	messenger := messaging.NewMessenger(serverID, event, redisCli)
	defer messenger.TearDown()

	appCtx := &provider.Context{
		Cache:                cache,
		Event:                event,
		UserRepo:             userRepo,
		CustomerRepo:         customerRepo,
		TicketRepo:           ticketRepo,
		OrderRepo:            orderRepo,
		AreaRepo:             areaRepo,
		TheaterRepo:          theaterRepo,
		TheaterTimeRepo:      theaterTimeRepo,
		ShowRepo:             showRepo,
		Storage:              storage,
		AccessTokenVerifier:  accessTokenVerifier,
		RefreshTokenVerifier: refreshTokenVerifier,
		Queue:                queue,
		Transactional:        transactional,
		Messenger:            messenger,
	}

	initJobHandlers(appCtx, cfg, queue)

	engine := gin.Default()
	web.InitHTTPRouters(appCtx, cfg, engine)

	hostAddress := fmt.Sprintf("%s:%s", cfg.Server.Host, cfg.Server.Port)
	srv := &http.Server{
		Addr:    hostAddress,
		Handler: engine,
	}

	go func() {
		logrus.Infof("%s running in %s mode at %s\n", cfg.AppName, cfg.Mode, hostAddress)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	logrus.Info("Gracefully shutting down server... [max delay 5 secs]")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		logrus.Fatal("Server forced to shutdown: ", err)
	}

	logrus.Println("Server exiting")
}

func getAccessAndRefreshTokenVerifier(cfg *config.Config) (*framework.JWTVerifier, *framework.JWTVerifier) {

	accessTokenVerifier, err := framework.NewJWTVerifier(cfg.JWT.AccessTokenKeyPath)
	if err != nil {
		accessTokenVerifier, err = framework.NewJWTVerifierWithPEM(cfg.JWT.AccessTokenKey)
		if err != nil {
			panic(errors.Wrap(err, "jwt access token - Please provide either valid pem private key path or pem data directly"))
		}
		logrus.Info("[JWT] access token private key retrieved from PEM data")
	} else {
		logrus.Info("[JWT] access token private key retrieved from PEM file")
	}

	refreshTokenVerifier, err := framework.NewJWTVerifier(cfg.JWT.RefreshTokenKeyPath)
	if err != nil {
		refreshTokenVerifier, err = framework.NewJWTVerifierWithPEM(cfg.JWT.RefreshTokenKey)
		if err != nil {
			panic(errors.Wrap(err, "jwt refresh token - Please provide either valid pem private key path or pem data directly"))
		}
		logrus.Info("[JWT] refresh token private key retrieved from PEM data")
	} else {
		logrus.Info("[JWT] refresh token private key retrieved from PEM file")
	}

	return accessTokenVerifier, refreshTokenVerifier
}
