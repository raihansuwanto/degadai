package cmd

import (
	"fmt"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"starter.co/api/config"
)

func getConfig() *config.Config {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("$HOME/")
	viper.AddConfigPath("/")
	viper.AddConfigPath(".")

	viper.BindEnv("mode", "APP_MODE")
	viper.BindEnv("appname", "APP_NAME")

	// Server listener address
	viper.BindEnv("server.host", "SERVER_HOST")
	viper.BindEnv("server.port", "SERVER_PORT")

	// Database address and credentials
	viper.BindEnv("database.host", "DB_HOST")
	viper.BindEnv("database.port", "DB_PORT")
	viper.BindEnv("database.username", "DB_USER")
	viper.BindEnv("database.password", "DB_PASSWORD")
	viper.BindEnv("database.name", "DB_NAME")

	// Redis config
	viper.BindEnv("redis.address", "REDIS_ADDRESS")
	viper.BindEnv("redis.password", "REDIS_PASSWORD")

	// Email config
	viper.BindEnv("email.host", "EMAIL_HOST")
	viper.BindEnv("email.port", "EMAIL_PORT")
	viper.BindEnv("email.username", "EMAIL_USERNAME")
	viper.BindEnv("email.password", "EMAIL_PASSWORD")

	// Alibaba OSS config
	viper.BindEnv("alibabaoss.endpoint", "OSS_ENDPOINT")
	viper.BindEnv("alibabaoss.accessid", "OSS_ACCESS_ID")
	viper.BindEnv("alibabaoss.accessSecret", "OSS_ACCESS_SECRET")
	viper.BindEnv("alibabaoss.bucketName", "OSS_BUCKET_NAME")

	// AWS S3 config
	viper.BindEnv("s3.bucketName", "AWS_S3_BUCKET_NAME")

	// AWS credentials
	viper.BindEnv("aws.region", "AWS_REGION")
	viper.BindEnv("aws.accessKeyID", "AWS_ACCESS_KEY_ID")
	viper.BindEnv("aws.secretAccessKey", "AWS_SECRET_ACCESS_KEY")
	viper.BindEnv("aws.sessionToken", "AWS_SESSION_TOKEN")

	// Local storage config
	viper.BindEnv("localstorage.basedir", "LOCAL_STORAGE_BASEDIR")
	viper.BindEnv("localstorage.publicBaseDir", "LOCAL_STORAGE_PUBLIC_BASEDIR")
	viper.BindEnv("localstorage.baseUrl", "LOCAL_STORAGE_BASE_URL")

	// JWT key config
	viper.BindEnv("jwt.accessTokenKeyPath", "JWT_ACCESS_TOKEN_KEY_PATH")
	viper.BindEnv("jwt.refreshTokenKeyPath", "JWT_REFRESH_TOKEN_KEY_PATH")
	viper.BindEnv("jwt.accessTokenKey", "JWT_ACCESS_TOKEN_KEY")
	viper.BindEnv("jwt.refreshTokenKey", "JWT_REFRESH_TOKEN_KEY")

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			logrus.Infof("config file not found, continuing. err =  %s\n", err.Error())
		} else {
			panic(err)
		}
	}

	var cfg config.Config
	if err := viper.Unmarshal(&cfg); err != nil {
		panic(err)
	}

	return &cfg
}

func createDatabaseConnection(config *config.Config) *gorm.DB {
	databaseHost := fmt.Sprintf("%s:%d", config.Database.Host, config.Database.Port)
	dbURL := fmt.Sprintf("%s:%s@(%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		config.Database.UserName,
		config.Database.Password,
		databaseHost,
		config.Database.Name)
	db, err := gorm.Open(mysql.Open(dbURL), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	if sqlDB, err := db.DB(); err != nil {
		sqlDB.SetMaxOpenConns(15)
		sqlDB.SetMaxIdleConns(10)
		sqlDB.SetConnMaxLifetime(time.Minute * 10)
	}
	return db
}

func createRedisClient(cfg *config.Config) *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:         cfg.Redis.Address,
		Password:     cfg.Redis.Password,
		DB:           0,
		PoolSize:     10,
		MinIdleConns: 3,
	})
}
