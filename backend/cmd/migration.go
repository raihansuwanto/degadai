package cmd

import (
	"fmt"
	"log"
	"os/exec"
)

func migrationCommand(verbose []string) {
	config := getConfig()

	connCredential := fmt.Sprintf("mysql://%s:%s@(%s:%v)/%s", config.Database.UserName, config.Database.Password, config.Database.Host, config.Database.Port, config.Database.Name)

	args := []string{"-path", config.Command.MigratePath, "-database", connCredential, "-verbose"}

	for _, v := range verbose {
		args = append(args, v)
	}

	cmd := exec.Command(config.Command.MigrateApp, args...)
	fmt.Println(cmd.String()) // print the inputted command

	out, err := cmd.CombinedOutput()

	if err != nil {
		fmt.Println(string(out))
		log.Fatal(err)
	}
	fmt.Println(string(out)) //print the output
}

func migrationUpCommand() {
	verbose := []string{"up"}

	migrationCommand(verbose)
}

func migrationDownCommand() {
	verbose := []string{"down", "-all"}

	migrationCommand(verbose)
}
