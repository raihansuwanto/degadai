package main

import (
	"starter.co/api/cmd"
)

func main() {
	cmd.Execute()
}
