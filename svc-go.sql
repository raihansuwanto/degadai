/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 8.0.23-0ubuntu0.20.04.1 : Database - svc-go-sample
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`svc-go-sample` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `svc-go-sample`;

/*Table structure for table `areas` */

DROP TABLE IF EXISTS `areas`;

CREATE TABLE `areas` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `areas` */

insert  into `areas`(`id`,`name`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'Bogor','2021-05-28 10:59:09','2021-05-28 10:59:09',NULL),
(2,'Jakarta','2021-05-28 10:59:15','2021-05-28 10:59:15',NULL),
(3,'Surabaya','2021-05-28 10:59:21','2021-05-28 10:59:21',NULL);

/*Table structure for table `customers` */

DROP TABLE IF EXISTS `customers`;

CREATE TABLE `customers` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `deleted_at` (`deleted_at`),
  KEY `email_2` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `customers` */

/*Table structure for table `orders` */

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint DEFAULT NULL,
  `ticket_id` bigint DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `deleted_at` (`deleted_at`),
  KEY `fk_order_ticket` (`ticket_id`),
  KEY `fk_order_user` (`user_id`),
  CONSTRAINT `fk_order_ticket` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`id`),
  CONSTRAINT `fk_order_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `orders` */

insert  into `orders`(`id`,`user_id`,`ticket_id`,`created_at`,`updated_at`,`deleted_at`) values 
(9,2,1,'2021-04-12 12:05:20','2021-04-12 12:05:20',NULL),
(10,1,1,'2021-04-18 03:49:51','2021-04-18 03:49:51',NULL),
(11,2,4,'2021-04-18 03:51:55','2021-04-18 03:51:55',NULL),
(12,2,3,'2021-04-18 03:59:11','2021-04-18 03:59:11',NULL),
(13,3,2,'2021-04-18 11:21:54','2021-04-18 11:21:54',NULL),
(14,4,5,'2021-04-18 21:23:20','2021-04-18 21:23:20',NULL),
(15,4,4,'2021-04-18 21:23:25','2021-04-18 21:23:25',NULL),
(16,4,2,'2021-04-18 21:39:58','2021-04-18 21:39:58',NULL),
(17,22,1,'2021-05-19 11:43:15','2021-05-19 11:43:15',NULL),
(18,23,1,'2021-05-20 12:58:58','2021-05-20 12:58:58',NULL),
(19,24,5,'2021-05-20 13:06:36','2021-05-20 13:06:36',NULL);

/*Table structure for table `schema_migrations` */

DROP TABLE IF EXISTS `schema_migrations`;

CREATE TABLE `schema_migrations` (
  `version` bigint NOT NULL,
  `dirty` tinyint(1) NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `schema_migrations` */

insert  into `schema_migrations`(`version`,`dirty`) values 
(11,0);

/*Table structure for table `shows` */

DROP TABLE IF EXISTS `shows`;

CREATE TABLE `shows` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `area_id` bigint DEFAULT NULL,
  `theater_id` bigint DEFAULT NULL,
  `theater_times_id` bigint DEFAULT NULL,
  `ticket_id` bigint DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `shows` */

insert  into `shows`(`id`,`area_id`,`theater_id`,`theater_times_id`,`ticket_id`,`date`,`created_at`,`updated_at`,`deleted_at`) values 
(1,NULL,1,1,4,'2021-06-04','2021-05-31 16:22:00','2021-05-31 16:22:00',NULL),
(2,NULL,1,2,4,'2021-06-04','2021-05-31 16:22:21','2021-05-31 16:22:21',NULL),
(3,NULL,1,1,1,'2021-06-03','2021-05-31 16:22:38','2021-05-31 16:22:38',NULL),
(4,NULL,1,2,1,'2021-06-03','2021-05-31 16:22:45','2021-05-31 16:22:45',NULL),
(5,NULL,1,1,2,'2021-06-04','2021-06-02 14:20:49','2021-06-02 14:20:49',NULL),
(6,NULL,1,2,2,'2021-06-04','2021-06-02 14:20:51','2021-06-02 14:20:51',NULL),
(7,NULL,1,1,3,'2021-06-03','2021-06-02 14:20:52','2021-06-02 14:20:52',NULL),
(8,NULL,1,2,3,'2021-06-03','2021-06-02 14:20:53','2021-06-02 14:20:53',NULL),
(9,NULL,1,1,5,'2021-06-05','2021-06-03 08:50:54','2021-06-03 08:50:54',NULL),
(10,NULL,1,2,5,'2021-06-05','2021-06-03 08:50:58','2021-06-03 08:50:58',NULL);

/*Table structure for table `theater_times` */

DROP TABLE IF EXISTS `theater_times`;

CREATE TABLE `theater_times` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `theater_id` bigint DEFAULT NULL,
  `time_start` time DEFAULT NULL,
  `time_end` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `theater_times` */

insert  into `theater_times`(`id`,`theater_id`,`time_start`,`time_end`,`created_at`,`updated_at`,`deleted_at`) values 
(1,1,'13:45:00',NULL,'2021-05-31 16:20:55','2021-05-31 16:20:55',NULL),
(2,1,'14:15:00',NULL,'2021-05-31 16:21:03','2021-05-31 16:21:03',NULL),
(3,1,'15:55:00',NULL,'2021-05-31 16:21:20','2021-05-31 16:21:20',NULL),
(4,1,'16:25:00',NULL,'2021-05-31 16:21:26','2021-05-31 16:21:26',NULL);

/*Table structure for table `theaters` */

DROP TABLE IF EXISTS `theaters`;

CREATE TABLE `theaters` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `area_id` int DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `telp` varchar(255) DEFAULT NULL,
  `address` text,
  `description` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `theaters` */

insert  into `theaters`(`id`,`area_id`,`name`,`telp`,`address`,`description`,`created_at`,`updated_at`,`deleted_at`,`start_time`) values 
(1,2,'ARION XXI','(021) 475 7658','Arion Plaza Lantai 4 \r\nJl. Pemuda Kav. 3-4 Rawamangun\r\nJakarta Timur','Senin s/d Kamis Rp25.000\r\nJumat Rp30.000 \r\nSabtu/Minggu/Libur Rp35.000','2021-05-31 14:07:06','2021-05-31 14:07:06',NULL,NULL),
(2,1,'ARTHA GADING XXI',NULL,NULL,NULL,'2021-06-01 18:56:43','2021-06-01 18:56:43',NULL,NULL),
(3,3,'ATRIUM XXI',NULL,NULL,NULL,'2021-06-01 18:56:49','2021-06-01 18:56:49',NULL,NULL),
(4,2,'BASSURA XXI',NULL,NULL,NULL,'2021-06-01 18:56:55','2021-06-01 18:56:55',NULL,NULL);

/*Table structure for table `tickets` */

DROP TABLE IF EXISTS `tickets`;

CREATE TABLE `tickets` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `area_id` int DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` text,
  `stock` int DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `deleted_at` (`deleted_at`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `tickets` */

insert  into `tickets`(`id`,`area_id`,`name`,`image`,`description`,`stock`,`date_start`,`date_end`,`created_at`,`updated_at`,`deleted_at`) values 
(1,1,'Maliq & D\'essentials','ticket/images/img-b3de2551-7c49-4f29-964b-f0ffad1ece55','bukan ticket konsermu',50,'2021-05-24','2021-06-02','2021-04-12 00:53:16','2021-05-28 08:51:08',NULL),
(2,1,'Efek Rumah Kaca','ticket/images/img-66c02d80-44fe-4a84-93de-8f527701bc64','Saksikanlah',50,'2021-06-01','2021-06-06','2021-04-13 14:06:26','2021-05-28 08:51:32',NULL),
(3,2,'Noah','ticket/images/img-d96538ea-7c68-4c00-98a8-57103d82cac0','ini ticket konser',50,'2021-05-24','2021-06-02','2021-04-16 23:00:37','2021-05-28 08:53:48',NULL),
(4,2,'Naif','ticket/images/img-c634f606-0aa5-4de9-bbb0-112030233864','ini tiket 2',5,'2021-05-24','2021-06-02','2021-04-17 01:31:04','2021-05-28 08:54:19',NULL),
(5,3,'Sheila On 7','ticket/images/img-720f5774-e979-4588-ba14-8c2f6f26d080','ini tiket konser',5,'2021-06-01','2021-06-06','2021-04-17 01:34:25','2021-05-28 08:54:35',NULL),
(6,NULL,'konser 6','','asdasd',5,NULL,NULL,'2021-04-18 21:40:22','2021-04-18 21:40:22','2021-04-18 22:17:00'),
(7,NULL,'tiket 7','','asdasd',5,NULL,NULL,'2021-04-18 22:01:41','2021-04-18 22:01:41','2021-04-18 22:07:36'),
(8,NULL,'abcabc','','abcqasdf',2,NULL,NULL,'2021-05-18 13:27:41','2021-05-18 13:27:41','2021-04-18 22:07:36'),
(9,NULL,'asdasd','','asdasd',5,NULL,NULL,'2021-05-18 13:29:11','2021-05-18 13:29:11','2021-04-18 22:07:36');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `serial` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `is_admin` tinyint DEFAULT '0',
  `email` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `serial` (`serial`),
  UNIQUE KEY `email` (`email`),
  KEY `deleted_at` (`deleted_at`),
  KEY `email_2` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`serial`,`name`,`password`,`is_admin`,`email`,`address`,`created_at`,`updated_at`,`deleted_at`) values 
(1,'usr-895b5a09-2772-4e11-9577-c8a5c0258959','admin','$2a$14$Z1rFv7/lahmIkbsfFjWrHejQm1xV/dP0xFAQuApSvwsjLlA.ZajDS',1,'admin@degadai.id',NULL,'2021-04-12 00:10:53','2021-04-12 00:10:53',NULL),
(2,'usr-0ec68c01-2f75-4ed9-a7f6-c7cf595ea272','raihan','$2a$14$JeaFpAhT8jwo32JVRu.YD.PztNpoIdLvFEpOe5InrGygmv0wZwtKq',0,'raihan@student.ub.ac.id','jl. jalan kesana kesini','2021-04-12 00:54:10','2021-04-12 16:51:51',NULL),
(3,'usr-6d6451a6-f2ef-42c5-a517-b94005a71223','raihan test 2','$2a$14$JUbv2pWBmDM6sjAA7ufPX.z4VY4ttMgT5ndZ4uvsz5sghCposv23e',0,'raihan@gmail.com','ini address','2021-04-18 11:21:31','2021-04-18 11:21:31',NULL),
(4,'usr-4ca2a3ec-2374-4da4-b8b8-1feef119af92','raihan test 3','$2a$14$38FUHne2lmS5E0n9ZCttyukSK.7sCHdL.enlj85UsI7GfBeZc9ATq',0,'raihanms@gmail.com','asdasd','2021-04-18 21:23:12','2021-04-18 21:23:12',NULL),
(5,'usr-37d8df9f-486c-4c44-a268-46a2202a6c92','ambon','$2a$14$Sb3nzXdcvi5a40TvELNOFefR31lUusqWX4SkyCBnCM7MwMknMceUy',0,'ambon@gmail.com','jl sana sini','2021-05-11 20:57:16','2021-05-11 20:57:16',NULL),
(7,'usr-85d7d25a-cd71-4ef2-b398-8209e17edaff','ambon','$2a$14$alClW6jZEbVG8ebFlpeDseI9nY4jrQrAvrwNQ50EYlF3wT1ZZiI5C',0,'ambon1@gmail.com','jl. satu dua','2021-05-12 01:06:29','2021-05-12 01:06:29',NULL),
(8,'usr-ffd8804d-7af1-461d-8911-9418065c70f8','ambon','$2a$14$i11BjSyMLVlJtmhvotoRTeXemtxY1pmFhyFTc4JNMvadkPvR3.cXO',0,'ambon2@gmail.com','j ljalan','2021-05-12 01:09:03','2021-05-12 01:09:03',NULL),
(9,'usr-79eabbb0-82ea-42c6-87c0-cbcbd008704c','ambon','$2a$14$4s28usT60ZR1FZ7kHdRfbeHI6kg6XX7vRf4PHTDpuHVaCoP68oZB2',0,'ambon3@gmail.com','jl jalan','2021-05-12 01:10:08','2021-05-12 01:10:08',NULL),
(10,'usr-e751e6e8-ed4c-4186-a08f-a1ac755a1400','ambon','$2a$14$cl5STdiwjwJrpTwCTvTpdOwv15ppTYQNI/hmrEQf2A0DLfOn4EUNu',0,'ambon4@gmail.com','jl jalan','2021-05-12 01:10:48','2021-05-12 01:10:48',NULL),
(12,'usr-16423db5-8b52-4a43-8f2f-208736f0bd52','ambon','$2a$14$jMSUmExG9VPT4vz9QOA1AuCnVHdSsTuxM6/pLdmyOBo7T5SrW443G',0,'ambon5@gmail.com','jalan jalan','2021-05-12 01:13:57','2021-05-12 01:13:57',NULL),
(13,'usr-826be5a4-97b5-4f42-b8de-3ed8e3b115cf','ambon','$2a$14$bUFTJ29B07CIWcHqlkvF4OvFrQh3v7IlzqWSu3fK4vjOxclObqQJO',0,'ambon7@gmail.com','jalan jalan','2021-05-12 01:52:35','2021-05-12 01:52:35',NULL),
(14,'usr-cb890a0f-fac0-48cb-af4c-7aaeaf847437','ambon','$2a$14$DYMjUs31nuzGPJq/FsFjKOG2vuA5VV7s6Djioo1xT2YJedHJjMayS',0,'ambon8@gmail.com','jalan jalan','2021-05-12 02:15:27','2021-05-12 02:15:27',NULL),
(15,'usr-a891e2a9-02bb-4fdd-929e-8914748f3cd9','ambon','$2a$14$3Mw8bCYNLRm8GGsOzAA4euUdAOdBari/PsgMulY5pSM.AKpK.E2EG',0,'ambon9@gmail.com','jalan jalan','2021-05-12 02:21:04','2021-05-12 02:21:04',NULL),
(16,'usr-353da27e-cc2c-4178-8a83-a8dbd812e31e','ambon','$2a$14$.oR4W3zRU7ioLbLOup/IM.oQSqgr8aR0L.y8qOX3nBrWVvukkOFqW',0,'ambon10@gmail.com','jalna jalan','2021-05-12 02:21:47','2021-05-12 02:21:47',NULL),
(17,'usr-b06cccf7-3ace-4958-af62-efe6e0a37155','ambion','$2a$14$ij3uKszHxYMmlRzxKvwNu.H23UhMeKpscfAuN3ud9BlGU/Pxl5Kcm',0,'ambon11@gmail.com','jaln jalan','2021-05-12 02:28:12','2021-05-12 02:28:12',NULL),
(18,'usr-7ec5940c-8df2-49ba-aa63-7a0cf3c22114','ambon','$2a$14$5xQ2xv9owH0luhPCRnc5hebuWEZqRMVR2BHuLvB7uJSMP3WDxgpey',0,'ambon99@gmail.com','jln jalan','2021-05-14 23:59:35','2021-05-14 23:59:35',NULL),
(19,'usr-c68698c0-c18d-4a22-b256-ee2344fc1b23','ambon','$2a$14$Yy30xkKk1QT7GFs1OAi7.OBNmDycAeAOTNs6D3WsxVzsaYp1JKme.',0,'ambon@degadai.id','jaln ambon','2021-05-18 01:39:10','2021-05-18 01:39:10',NULL),
(20,'usr-c9a55f9d-f7a0-462e-9a04-5d7d1a774625','budi','$2a$14$PsmLALiyCcusRw8PxJ4W0uUAvo7PAuknMbtReVRZfdprIJBGOszRy',0,'budi@gmail.com','pasarminggu','2021-05-18 13:21:39','2021-05-18 13:21:39',NULL),
(21,'usr-36a85b5b-c873-491a-ab0a-450f08d55448','budi','$2a$14$uSp0aiTbFynrsGyzNy.Cw.nN1qvSfzi.DSNbjaJnYRKkOQ.0J12eS',0,'budi2@gmail.com','jalan jalan','2021-05-18 13:22:55','2021-05-18 13:22:55',NULL),
(22,'usr-6465bf09-c35f-46b3-bf15-f36326b1c95e','raihan','$2a$14$p7zc40M2REvUYQTQqWohVeGHzIWbINOK7vPxf8vsA8TdFzACZ2OKu',0,'raihanxxx@gmail.com','jlan jalan ','2021-05-19 11:38:57','2021-05-19 11:38:57',NULL),
(23,'usr-4e4f8184-1414-40bd-8738-ab16658f9da6','ambon','$2a$14$MrFW2antIZPEUzrT2b5X8uFOXwgbz7Yk3A7EAPt8zBzKbOkrXNjbu',0,'ambonxx@gmail.com','jalan jalan','2021-05-20 12:58:42','2021-05-20 12:58:42',NULL),
(24,'usr-086fc8b7-946d-453a-8aad-2c0ae6304540','new user','$2a$14$tgDPkmnSiGkpA8tH1ckxeO2gKUltJwpuhiNyjewN.lV4yUyP0nU1i',0,'user2@gmail.id','jalan jalan','2021-05-20 13:06:11','2021-05-20 13:06:11',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
