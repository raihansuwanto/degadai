import RoutePath from "../RoutePath";

export const MenuItemsAdmin = [
    {
        title: 'Tiket',
        url: RoutePath.TICKET_INDEX,
        cName: 'nav-links'
    },
    {
        title: 'Order',
        url: RoutePath.CUSTOMER_ORDER,
        cName: 'nav-links'
    }
]