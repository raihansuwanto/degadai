import RoutePath from "../RoutePath";

export const MenuItemsCustomer = [
    {
        title: 'Tiket',
        url: RoutePath.TICKET_CUSTOMER_INDEX,
        cName: 'nav-links'
    },
    {
        title: 'Order',
        url: RoutePath.TICKET_CUSTOMER_ORDER,
        cName: 'nav-links'
    }
]