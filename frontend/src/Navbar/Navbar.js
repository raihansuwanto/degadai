import React, { Component } from 'react'
import { MenuItemsAdmin } from "./MenuItemsAdmin";
import { Link } from "react-router-dom";
import { Button } from "./Button";
import './Navbar.css';
import RoutePath from "../RoutePath";
import { removeUserSession,getToken, getUser } from '../Utils/Common';
import { MenuItemsCustomer } from './MenuItemsCustomer';
import { Container, Nav, Navbar as NvB } from 'react-bootstrap';
import { Link as LinkScroll, animateScroll as scroll } from "react-scroll";

const handleLogout = () => {
    removeUserSession();
  }

class Navbar extends Component {
    constructor(props) {
        super(props);
        this.state = {
          user: getUser(),
          nav:false
        }
      }
    state = { clicked: false }

    componentDidMount() {
        window.addEventListener("scroll", this.handleScroll);
      }
      componentWillUnmount() {
         window.removeEventListener('scroll');
       }

    handleScroll= () => {
        if (window.pageYOffset > 86) {
            if(!this.state.nav){
              this.setState({ nav: true });
            }
        }else{
            if(this.state.nav){
              this.setState({ nav: false });
            }
        }
    }

    handleMenu = () => {
        if(RoutePath.TICKET_CUSTOMER_ORDER != window.location.pathname){
            window.location.replace(RoutePath.TICKET_CUSTOMER_ORDER);
            var elem = document.getElementById("now-playing");
            elem.scrollIntoView();
        }
    }

    currentNavbar = () => {
        if(this.state.user.is_admin){
            return(
                <nav className="NavbarItems">
                    <h1 className="navbar-logo">React<i className="fab fa-react"></i></h1>
                    <div className="menu-icon" onClick={this.handleClick}>
                        <i className={this.state.clicked ? 'fas fa-times' :  'fas fa-bars '}></i>
                    </div>
                    <ul className={this.state.clicked ? 'nav-menu active' : 'nav-menu'}>
                        {MenuItemsAdmin.map((item,index) => {
                            return (
                                <li key={index}>
                                    <a className={item.cName} href={item.url}>
                                    {item.title}
                                    </a>
                                </li>
                            )
                        })}
                    <li key="logout">
                        <a className="nav-links-logout" onClick={handleLogout} href={RoutePath.LOGIN}>
                        Log Out
                        </a>
                    </li>
                    </ul>
                </nav>
            )
        }else{
            return(
                <nav className="NavbarItems">
                    <h1 className="navbar-logo">React<i className="fab fa-react"></i></h1>
                    <div className="menu-icon" onClick={this.handleClick}>
                        <i className={this.state.clicked ? 'fas fa-times' :  'fas fa-bars '}></i>
                    </div>
                    <ul className={this.state.clicked ? 'nav-menu active' : 'nav-menu'}>
                        {MenuItemsCustomer.map((item,index) => {
                            return (
                                <li key={index}>
                                    <a className={item.cName} href={item.url}>
                                    {item.title}
                                    </a>
                                </li>
                            )
                        })}
                    </ul>
                </nav>
            )
        }
    }

    render() {
        return (
            <>
            <header id="header" className="sticky-style-2">
                <div className="header-box">
                    <div className="container clearfix">
                        <div className="row align-items-center">
                            <div className="col-6">
                                <a href="https://21cineplex.com/" className="link-home">
                                    <img src="https://21cineplex.com//theme/v5/assets/img/icons/home@2x.png" alt="Home Icon"/>
                                </a>

                                <div id="logo">
                                    <a href="https://21cineplex.com/" className="standard-logo">
                                        <img src="https://21cineplex.com//theme/v5/assets/img/logo.png" alt="21 Cineplex Logo"/>
                                    </a>
                                </div>
                            </div>

                            <div className="col-6">
                                <form className="search-header" action="#" method="get">
                                    {/* <label for="search-movie">Search</label> */}
                                    <input type="text" name="search-movie" id="search-movie" placeholder="Search theater, movies..." class="ui-autocomplete-input" autocomplete="off"/>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
            </header>
            <NvB className="navbar" fixed={this.state.nav ? 'top' :  ''} bg="light" variant="light">
                {/* <Nav className="mr-auto"> */}
                <Container>
                <ul className="list-menu">
                    <li>
                        <LinkScroll
                            className="nav-link"
                            activeClass="active"
                            to="now-playing"
                            spy={true}
                            smooth={true}
                            offset={-70}
                            duration={500}
                            onClick={this.handleMenu}
                        >Now Playing</LinkScroll>
                    </li>
                    <li>
                        <LinkScroll
                            className="nav-link"
                            activeClass="active"
                            to="upcoming"
                            spy={true}
                            smooth={true}
                            offset={-70}
                            duration={500}
                            onClick={this.handleMenu}
                        >Upcoming</LinkScroll>
                    </li>
                    <li>
                        <a
                            className="nav-link"
                            activeClass="active"
                            href={RoutePath.THEATERS}
                        >Theaters</a>
                    </li>
                </ul>
            </Container>
            </NvB>
            </>
        )
    }
}

export default Navbar 