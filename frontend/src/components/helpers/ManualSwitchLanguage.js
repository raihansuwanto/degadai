import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Cookie from "js-cookie";

class ManualSwitchLanguage extends PureComponent {
    constructor(props) {
        super(props);
        this.multipleKey = this.multipleKey.bind(this);
        this.findingData = this.findingData.bind(this);
    }

    multipleKey(total_split, value, local_lang) {
        if (total_split.length > 2) {
            total_split.shift();
            return this.multipleKey(total_split, value[total_split[0]], local_lang);
        } else {
            if (
                typeof value[total_split[0]] !== 'undefined' &&
                value[total_split[0]] !== undefined &&
                value[total_split[0]] !== null &&
                value[total_split[0]] !== ''
            ) return value[total_split[0]][total_split[1]] === local_lang;
            else {
                if (
                    typeof value[total_split[1]] !== 'undefined' &&
                    value[total_split[1]] !== undefined &&
                    value[total_split[1]] !== null &&
                    value[total_split[1]] !== ''
                ) {
                    return value[total_split[1]] === local_lang;
                } else return false;
            }
        }
    }

    findingData(lang_split, local_lang) {
        let result;
        if (lang_split.length > 2) {
            result = this.props.data.find((value, index, obj) => this.multipleKey(lang_split, value, local_lang));
            if (
                typeof result !== 'undefined' &&
                result !== undefined &&
                result !== null &&
                result !== ''
            ) return result;
            else return this.props.data.find((value, index, obj) => this.multipleKey(lang_split, value, 'en'));
        } else {
            result = this.props.data.find((value, index, obj) => value[this.props.langAttr] === local_lang);
            if (
                typeof result !== 'undefined' &&
                result !== undefined &&
                result !== null &&
                result !== ''
            ) return result;
            else return this.props.data.find((value, index, obj) => value[this.props.langAttr] === 'en');
        }
    }

    render() {
        let local_lang = Cookie.get('language_code');
        let lang_split = this.props.langAttr.split('.');
        lang_split.unshift('test');
        if (
            typeof local_lang !== 'undefined' &&
            local_lang !== undefined &&
            local_lang !== null &&
            local_lang !== ''
        ) {
            let name = this.findingData(lang_split, local_lang);
            if (
                typeof name !== 'undefined' &&
                name !== undefined &&
                name !== null &&
                name !== ''
            ) return <>{name[this.props.valueAttr]}</>;
            else return null;
        } else {
            let name = this.findingData(lang_split, 'en');
            if (
                typeof name !== 'undefined' &&
                name !== undefined &&
                name !== null &&
                name !== ''
            ) return <>{name[this.props.valueAttr]}</>;
            else return null;
        }
    }
}

ManualSwitchLanguage.propTypes = {
    data: PropTypes.array.isRequired,
    langAttr: PropTypes.string,
    valueAttr: PropTypes.string,
};
ManualSwitchLanguage.defaultProps = {
    langAttr: 'code',
    valueAttr: 'name'
};

export default ManualSwitchLanguage;