import Cookie from "js-cookie";
import RoutePath from "../../RoutePath";

const Logout = () => {
    Cookie.remove('access_token');
    Cookie.remove('refresh_token');
    window.location.href = RoutePath.LOGIN;
}

export default Logout;