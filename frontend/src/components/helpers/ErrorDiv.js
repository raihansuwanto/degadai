const ErrorDiv = ({error}) => {
    return error ? <div className="alert alert-danger p-2 mt-2" role="alert">{error}</div> : null;
}

export default ErrorDiv