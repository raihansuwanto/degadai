import {PureComponent} from 'react';
import PropTypes from 'prop-types';

class UcFirst extends PureComponent {
    ucWords(string) {
        if (typeof string === 'string') return string.replace(/_/g, ' ').toLowerCase().replace(/\b[a-z]/g, letter => letter.toUpperCase());
        else return '';             
    }

    render() {
        return this.ucWords(this.props.text)
    }
}

UcFirst.propTypes = {
    text: PropTypes.string.isRequired
};

export default UcFirst;