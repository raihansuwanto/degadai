// if (typeof window === 'undefined') {
//     crypto = require("crypto").webcrypto;
// }

const maxIterations = 200000;

const hash256 = async (data) => {
    if (typeof data === 'string' || data instanceof String) {
        data = new TextEncoder("utf-8").encode(data);
    }
    
    return await crypto.subtle.digest("SHA-256", new TextEncoder("utf-8").encode(data));
}

const hash512 = async (data) => {
    if (typeof data === 'string' || data instanceof String) {
        data = new TextEncoder("utf-8").encode(data);
    }

    return await crypto.subtle.digest("SHA-512", data);
}

const generateSalt = (size) => {
    return crypto.getRandomValues(new Uint8Array(size));
}

const generateKeyWithIV = async (password, salt, usage) => {
    const passwordBytes = new TextEncoder("utf-8").encode(password);
    const baseKey = await crypto.subtle.importKey("raw", passwordBytes, {name: "PBKDF2"}, false, ["deriveBits"]);
    var pbkdf2Bytes = await crypto.subtle.deriveBits({
        name: "PBKDF2",
        salt: salt,
        iterations: maxIterations,
        hash: "SHA-512"
    }, baseKey, 384);
    pbkdf2Bytes = new Uint8Array(pbkdf2Bytes);

    var keyBytes = pbkdf2Bytes.slice(0, 32);
    var ivBytes = pbkdf2Bytes.slice(32);

    const key = await crypto.subtle.importKey("raw", keyBytes, {
        name: "AES-GCM",
        length: 256
    }, false, [usage]);

    return {
        key, iv: ivBytes
    }
}

const encryptRawData = async (password, salt, plaintextData) => {
    const {key, iv} = await generateKeyWithIV(password, salt, "encrypt");
    const ciphertext = await crypto.subtle.encrypt({name: "AES-GCM", iv: iv}, key, plaintextData);
    return ciphertext;
}

const decryptRawData = async (password, salt, ciphertextData) => {
    const {key, iv} = await generateKeyWithIV(password, salt, "decrypt");
    const plaintext = await crypto.subtle.decrypt({name: "AES-GCM", iv: iv}, key, ciphertextData);
    return plaintext;
}

const _readFile = (file) => {
    return new Promise((resolve, reject) => {
        var fr = new FileReader();
        fr.onload = () => {
            resolve(fr.result )
        };
        fr.readAsArrayBuffer(file);
    });
}

const arrayBufferToBase64 = (buffer) => {
    var binary = '';
    var bytes = new Uint8Array(buffer);
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i]);
    }
    return btoa(binary);
}

const base64ToUint8Array = (base64String) => {
    return Uint8Array.from(atob(base64String), c => c.charCodeAt(0));
}

/**
 * encrypt data string
 * 
 * @param {string} password 
 * @param {string|ArrayBuffer|ArrayBufferView} plaintextData 
 * @returns {string} encrypted data encoded such: {salt}|{ciphertext}
 */
 const encryptData = async (password, plaintextData) => {
    if (typeof plaintextData === 'string' || plaintextData instanceof String) {
        plaintextData = new TextEncoder().encode(plaintextData);
    }

    const salt = generateSalt(16);
    const ciphertext = await encryptRawData(password, salt, plaintextData);
    return arrayBufferToBase64(salt) + "|" + arrayBufferToBase64(ciphertext);
}

/**
 * decrypt data string
 * 
 * @param {string} password 
 * @param {string} encodedCiphertext string encoded {salt}|{ciphertext}
 * @returns {string} plaintext string
 */
const decryptData = async (password, encodedCiphertext) => {
    var [salt, ciphertext] = encodedCiphertext.split("|");
    salt = base64ToUint8Array(salt);
    ciphertext = base64ToUint8Array(ciphertext);
    const plaintext = await decryptRawData(password, salt, ciphertext)
    return new TextDecoder().decode(plaintext);
}

/**
 * encrypt file or blob with given key derived from password
 * 
 * @param {string} password 
 * @param {File} file 
 * @returns {Blob} encrypted blob
 */
const encryptFile = async (password, file) => {
    if (!(file instanceof Blob)) {
        throw TypeError("Err: file should be instance of 'File' or 'Blob'");
    }

    const plaintextData = await _readFile(file);
    const salt = generateSalt(16);
    var ciphertext = await encryptRawData(password, salt, plaintextData);
    ciphertext = new Uint8Array(ciphertext);

    const resultCiphertext = new Uint8Array(ciphertext.length + 16);
    resultCiphertext.set(salt, 0);
    resultCiphertext.set(ciphertext, 16);

    return new Blob([new Uint8Array(resultCiphertext)], {type: "application/octet-stream"});
}

/**
 * decrypt file or blob with given key derived from password
 * 
 * @param {string} password 
 * @param {File} file 
 * @returns {Blob} encrypted blob
 */
const decryptFile = async (password, file) => {
    if (!(file instanceof Blob)) {
        throw TypeError("Err: file should be instance of 'File' or 'Blob'");
    }

    var ciphertext = await _readFile(file);
    ciphertext = new Uint8Array(ciphertext);
    
    const salt = ciphertext.slice(0, 16);
    ciphertext = ciphertext.slice(16);

    const plaintext = await decryptRawData(password, salt, ciphertext);
    return new Blob([new Uint8Array(plaintext)], {type: "application/octet-stream"});
}


export default {
    hash256,
    hash512,
    generateSalt,
    arrayBufferToBase64,
    base64ToUint8Array,
    encryptRawData,
    encryptData,
    decryptData,
    encryptFile,
    decryptFile
}