import React, {PureComponent} from 'react';
import {NavLink, withRouter, matchPath} from "react-router-dom";
import Logout from '../helpers/Logout';
import RoutePath from '../../RoutePath';
import './Sidebar.css'
import { removeUserSession,getToken, getUser } from '../../Utils/Common';


const handleLogout = () => {
    removeUserSession();
  }

const menu = [
    {
        name: "Ticket",
        path: RoutePath.TICKET_INDEX,
        icon: <i class="fas fa-ticket-alt"></i>,
        submenu: false,
    },
    {
        name: "Theater",
        path: RoutePath.THEATER_INDEX,
        icon: <i class="fas fa-photo-video"></i>,
        submenu: false,
    },
    // {
    //     name: "Task Management",
    //     path: AppRoutePath.TASK,
    //     icon: <i className={"fas fa-clipboard"}></i>,
    //     submenu: [
    //         {
    //             name: "Task",
    //             path: AppRoutePath.TASK
    //         },
    //     ],
    // },
    // {
    //     name: "Users",
    //     path: AppRoutePath.USERS,
    //     icon: <i className={"fas fa-user"}></i>,
    //     submenu: [
    //         {
    //             name: "Advisor",
    //             path: AppRoutePath.USERS_ADVISOR
    //         },
    //         {
    //             name: "Affiliate",
    //             path: AppRoutePath.USERS_AFFILIATE
    //         },
    //         {
    //             name: "Lead Customer",
    //             path: AppRoutePath.USERS_LEAD_CUSTOMER
    //         },
    //         {
    //             name: "Customer",
    //             path: AppRoutePath.USERS_CUSTOMER
    //         },
    //     ],
    // },
];

const CustomNavLink = ({me, to, icon, name, submenu})=>{
    return submenu ? 
        <div
            className={`text-decoration-none text-white btn-block px-3 py-3 d-flex align-items-center pointer ${matchPath(me.props.match.path, to) && `active`}`}
            onClick={() => me.setSubmenu(name, submenu)}
        >
            {icon ? <>{icon}&nbsp;&nbsp;&nbsp;</> : <i className={"fas fa-cog"}></i>}
            <div className="d-flex justify-content-between w-100">
                <div className="font-weight-bold">
                    {name}
                </div>
                <div>
                    <i className="fas fa-caret-right" style={{color: '#ABC7DB'}}></i>
                </div>
            </div>
        </div> :
        <NavLink
            to={to}
            className="text-decoration-none text-white btn-block px-3 py-3 d-flex align-items-center"
            activeClassName="font-weight-bold py-3 active"
        >
            {icon ? <>{icon}&nbsp;&nbsp;&nbsp;</> : <i className={"fas fa-cog"}></i>}
            <div className="d-flex justify-content-between w-100">
                <div className="font-weight-bold">
                    {name}
                </div>
            </div>
        </NavLink>
}

const SidebarFooter = ({me}) => {
    return (
        <div className="d-flex w-100 bgc-3075A6">
            <NavLink to={RoutePath.DASHBOARD} className="d-flex justify-content-center text-decoration-none text-white btn-block p-3 m-0">
                <i className="fas fa-bell"></i>
            </NavLink>
            <NavLink to={RoutePath.DASHBOARD} className="d-flex justify-content-center text-decoration-none text-white btn-block p-3 m-0">
                <i className="fas fa-user-circle"></i>
            </NavLink>
            <NavLink to={RoutePath.LOGIN} onClick={handleLogout} className="d-flex justify-content-center text-decoration-none text-white btn-block p-3 m-0">
                <i className="fas fa-sign-out-alt"></i>
            </NavLink>
        </div>
    )
}

const Menu = ({me}) => {
    return (
        <div id="sidebar-wrapper" className="bgc-3075A6 position-relative" style={{width: '20rem', overflow: 'auto', minHeight: '100vh'}}>
            <div className="sidebar-heading d-flex align-items-center justify-content-center">
                <h2 className="m-0 text-white font-weight-bold">CINEMA 21</h2>
            </div>
            <div className="list-group sidebar-wacvr mx-2 mb-5">
                {
                    menu.map((value, index) => (
                        <div key={index}>
                            <CustomNavLink me={me} to={value.path} name={value.name} icon={value.icon} submenu={value.submenu} />
                        </div>
                    ))
                }
            </div>
            <div className="position-absolute w-100" style={{bottom: 0}}>
                <SidebarFooter me={me} />
            </div>
        </div>
    )
}

const SubMenu = ({me, title, submenu}) => {
    return (
        <div id="sidebar-wrapper" className="d-flex position-relative" style={{width: '20rem', overflow: 'auto', minHeight: '100vh'}}>
            <div className="bgc-3075A6" style={{width: '20%'}}>
                <div className="sidebar-heading d-flex align-items-center justify-content-center">
                    <i className={"fas fa-bullseye text-white"} style={{fontSize: 32}}></i>
                </div>
                <div className="mt-3">
                    {
                        menu.map(value => (
                            <div className="text-white btn-block py-3 d-flex justify-content-center" style={value.name === title ? {backgroundColor: '#ABC7DB'} : null}>
                                {value.icon ? <>{value.icon}</> : <i className={"fas fa-cog"}></i>}
                            </div>
                        ))
                    }
                </div>
            </div>
            <div className="bgc-438EBC mb-5" style={{width: '80%'}}>
                <div className="sidebar-heading d-flex align-items-center pointer" onClick={() => me.backSubmenu()}>
                    <div className="ml-1">
                        <i className="fas fa-caret-left" style={{color: '#ABC7DB'}}></i>
                    </div>
                    <div className="ml-3">
                        <h5 className="text-white font-weight-bold m-0">{title}</h5>
                    </div>
                </div>
                <div className="mt-3 mx-2">
                    {
                        submenu && Array.isArray(submenu) && submenu.map(value => (
                            <NavLink
                                to={value.path}
                                className="text-decoration-none font-weight-bold btn-block py-2 pl-3 d-flex"
                                style={matchPath(me.props.match.path, value.path) ?
                                    {backgroundColor: '#FFFFFF', color: '#3075A6', borderRadius: '0.25rem', boxShadow: '0 3px 6px #00000029'} :
                                    {color: '#FFFFFF'}
                                }
                            >
                                <div>{value.name}</div>
                            </NavLink>
                        ))
                    }
                </div>
            </div>
            <div className="position-absolute w-100" style={{bottom: 0}}>
                <SidebarFooter me={me} />
            </div>
        </div>
    )
}

class Sidebar extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            sub_menu_toggle: false,
            sub_menu_title: '',
            sub_menu_data: [],
        }
    }

    handleLogout = async() => {
        Logout();
    }
    
    setSubmenu = (title, submenu) => {
        this.setState({
            sub_menu_toggle: true,
            sub_menu_title: title,
            sub_menu_data: submenu,
        });
    }

    backSubmenu = () => {
        this.setState({
            sub_menu_toggle: false,
        });
    }

    render() {
        return this.state.sub_menu_toggle ? <SubMenu me={this} title={this.state.sub_menu_title} submenu={this.state.sub_menu_data} /> : <Menu me={this} />
    }
}

export default withRouter(Sidebar);