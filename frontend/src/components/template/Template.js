import React, {PureComponent} from "react";
import PropTypes from "prop-types";
import IsEmpty from "../helpers/IsEmpty";
import Cookie from "js-cookie";
import {matchPath} from "react-router-dom";
import Sidebar from "./Sidebar";

class Template extends PureComponent {
    render() {
        // if (IsEmpty(Cookie.get('access_token')) && IsEmpty(Cookie.get('refresh_token')) && !matchPath(window.location.pathname, AppRoutePath.LOGIN)) {
        //     window.location.href = AppRoutePath.LOGIN;
        // } else {
            return (
                <div className="">
                    <div className="d-flex" id="wrapper" style={{overflow: 'auto'}}>
                        <Sidebar />
                        <div className="ohana-container">
                            {this.props.children}
                        </div>
                    </div>
                </div>
            );
        // }
    }
}

Template.propTypes = {
    withPadding: PropTypes.bool,
    withFooter: PropTypes.bool,
};
Template.defaultProps = {
    withPadding: true,
    withFooter: true,
};

export default Template;