import React, { PureComponent } from 'react';

class Navbar extends PureComponent {
    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bgc-F9F9F9 border-bottom" style={{height: '5rem'}}>
                {this.props.children}
            </nav>
        );
    }
}

export default Navbar;