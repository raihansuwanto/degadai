import axios from "axios";
import Config from "./Config";
import Cookie from "js-cookie";
import IsEmpty from "../helpers/IsEmpty";

export const AxiosCustomGet = (url, config, callback, errorCallback, finallyCallback) => {
    axios.get(url, config).then(response => {
        if (callback) callback(response);
    }).catch(error => {
        AxiosCustomError(error, errorCallback, finallyCallback);
    }).finally(() => {
        if (finallyCallback) finallyCallback();
    });
};

export const AxiosCustomPost = (url, data, config, callback, errorCallback, finallyCallback) => {
    axios.post(url, data, config).then(response => {
        if (callback) callback(response);
    }).catch(error => {
        AxiosCustomError(error, errorCallback, finallyCallback);
    }).finally(() => {
        if (finallyCallback) finallyCallback();
    });
};

export const AxiosCustomPut = (url, data, config, callback, errorCallback, finallyCallback) => {
    axios.put(url, data, config).then(response => {
        if (callback) callback(response);
    }).catch(error => {
        AxiosCustomError(error, errorCallback, finallyCallback);
    }).finally(() => {
        if (finallyCallback) finallyCallback();
    });
};

export const AxiosCustomDelete = (url, config, callback, errorCallback, finallyCallback) => {
    axios.delete(url, config).then(response => {
        if (callback) callback(response);
    }).catch(error => {
        AxiosCustomError(error, errorCallback, finallyCallback);
    }).finally(() => {
        if (finallyCallback) finallyCallback();
    });
};

const AxiosCustomError = (error, errorCallback, finallyCallback) => {
    if (error.response) {
        if (error.response.status === 401) {
            axios.get(`${process.env.REACT_APP_BASE_API_URL}auth/refresh`, Config({
                Authorization: `Bearer ${Cookie.get('refresh_token')}`
            })).then(response => {
                Cookie.set('access_token', response.data.data.access_token, {expires: 2});
                if (!IsEmpty(response.data.data.refresh_token)) {
                    Cookie.set('refresh_token', response.data.data.refresh_token, {expires: 30});
                }
            }).catch(error => {
                if (errorCallback) errorCallback(error);
            }).finally(() => {
                if (finallyCallback) finallyCallback();
            });
        } else if (errorCallback) errorCallback(error);
    } else if (errorCallback) errorCallback(error);
}