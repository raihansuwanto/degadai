/**
 * http client that support access and refresh token mechanism
 * this package needs to abstract away how refresh token is use
 * when an api's access token is expire and resume the api call
 * so the caller does not need to know anything about this mechanism
 * 
 */
import axios from "axios";
import Cookies from "js-cookie";

const HTTPUnauthorized = 401
const REFRESH_TOKEN_ENDPOINT = process.env.REACT_APP_BASE_API_URL+"/auth/refresh";

// TODO fix: race condition/multiple refresh token will be perform if api call called at the same time
const _apiCallRunner = async (apiCall) => {
    try {
        const resp = await apiCall(Cookies.get("access_token"));
        return resp;
    } catch (err) {
        const errResp = err.response;
        if (!errResp) {
            throw err;
        }

        if (errResp.status == HTTPUnauthorized && errResp.headers["token-expire"]) {
            console.log("[i] response unauth, token expire, need refresh");

            // if error happened here it would be thrown up to the caller
            const refreshResp = await axios.get(REFRESH_TOKEN_ENDPOINT, {
                headers: {
                    Authorization: "Bearer " + Cookies.get("refresh_token")
                }
            });
            const {access_token, refresh_token} = refreshResp.data.data;
            Cookies.set("access_token", access_token, {expires: 2});

            if (refresh_token) {
                Cookies.set("refresh_token", refresh_token);
            }

            return apiCall(Cookies.get("access_token"));
        } else {
            throw err;
        }
    }
}

const GET = async (url) => {
    const apiCall = async (accessToken) => {
        return axios.get(url, {
            headers: {
                "Authorization": "Bearer " + accessToken
            }
        });
    };

    return await _apiCallRunner(apiCall);
}

const POST = async (url, data) => {
    const apiCall = async (accessToken) => {
        return axios.post(url, data, {
            headers: {
                "Authorization": "Bearer " + accessToken
            }
        });
    }

    return await _apiCallRunner(apiCall);
}

export default {
    GET,
    POST
}