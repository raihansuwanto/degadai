import React, { useState, useEffect, useMemo, useRef } from "react";
import axios from "axios";

// export const Theater = ({ onSubmit }) => {

 



//   return (
//     <form onSubmit={onSubmit}>
//       <div className="form-group">
//         <label htmlFor="name">Name</label>
//         <input className="form-control" required id="name" />
//       </div>
      // <div className="form-group">
      //   <label htmlFor="area">Area</label>
      //   <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
      //     <option selected>Choose...</option>
      //     {
      //       areas.map((area) => 
      //         <option value={area.ID}>{area.name}</option>
      //       )
      //     }
      //   </select>
      // </div>
//       <div className="form-group">
//         <label htmlFor="address">Address</label>
//         <input
//           className="form-control"
//           id="address"
//           required
//         />
//       </div>
//       <div className="form-group">
//         <label htmlFor="phone">Phone</label>
//         <input
//           className="form-control"
//           id="phone"
//         />
//       </div>
//       <div className="form-group">
//         <button className="form-control btn btn-primary" type="submit">
//           Submit
//         </button>
//       </div>
//     </form>
//   );
// };
// export default Theater;

export const Theater = ({ onSubmit }) => {

const [areas, setAreas] = useState([]);

const retrieveAreas = () => {
axios.get(`${process.env.REACT_APP_BASE_API_URL}area`, {
  })
  .then(response => {
      setAreas(response.data.data);
      console.log(response.data.data);
  })
  .catch(error => {
      console.log(error)
  });
    };

useEffect(() => {
  retrieveAreas();
}, []);

  return (
    <form onSubmit={onSubmit}>
      <div className="form-group">
        <label htmlFor="name">Name</label>
        <input 
          className="form-control" 
          id="name" 
          required
          />
      </div>
      <div className="form-group">
        <label htmlFor="description">Address</label>
        <input
          className="form-control"
          id="address"
          required
        />
      </div>
      <div className="form-group">
        <label htmlFor="telp">No Telp</label>
        <input
          className="form-control"
          id="telp"
          required
        />
      </div>
      <div className="form-group">
        <label htmlFor="area">Area</label>
        <select class="custom-select my-1 mr-sm-2" id="area">
          <option selected>Choose...</option>
          {
            areas.map((area) => 
              <option value={area.ID}>{area.name}</option>
            )
          }
        </select>
      </div>
      <div className="form-group">
        <button className="form-control btn btn-primary" type="submit">
          Submit
        </button>
      </div>
      
    </form>
  );
};
export default Theater;
