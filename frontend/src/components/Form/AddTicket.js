import React from 'react';

export const Ticket = ({ onSubmit }) => {
  return (
    <form onSubmit={onSubmit}>
      <div className="form-group">
        <label htmlFor="name">Name</label>
        <input 
          className="form-control" 
          id="name" 
          required
          />
      </div>
      <div className="form-group">
        <label htmlFor="description">Description</label>
        <input
          className="form-control"
          id="description"
          required
        />
      </div>
      <div className="form-group">
        <label htmlFor="stock">Stock</label>
        <input
          className="form-control"
          id="stock"
          required
        />
      </div>
      <div className="form-group">
        <button className="form-control btn btn-primary" type="submit">
          Submit
        </button>
      </div>
    </form>
  );
};
export default Ticket;
