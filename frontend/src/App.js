import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { lazy, Suspense } from 'react';
import RoutePath from './RoutePath';
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState } from 'react';
import PrivateRoute from './Utils/PrivateRoute';
import PublicRoute from './Utils/PublicRoute';
import Navbar from "./Navbar/Navbar"

const Dashboard = lazy(() => import('./pages/Dashboard'))
const Customer = lazy(() => import('./pages/customer/CustomerRoute'))
const Ticket = lazy(() => import('./pages/ticket/TicketRoute'))
const TicketDetail = lazy(() => import('./pages/ticket/TicketDetail'))
const Login = lazy(() => import('./pages/login/Login'))
const Register = lazy(() => import('./pages/login/Register'))
const DashboardAdmin = lazy(() => import('./pages/dashboard/DashboardAdmin'))
const DashboardCustomer = lazy(() => import('./pages/dashboard/DashboardCustomer'))
const TicketCustomer = lazy(() => import('./pages/ticketCustomer/TicketCustomerRoute'))
const TicketCustomerDetail = lazy(() => import('./pages/ticketCustomer/TicketCustomerDetail'))
const CustomerOrder = lazy(() => import('./pages/customer/CustomerOrder'))
const TicketHome = lazy(() => import('./pages/ticketCustomer/TicketCustomerOrder'))
const Theaters = lazy(() => import('./pages/theaters/Theaters'))
const TheaterDetail = lazy(() => import('./pages/theaters/TheaterDetail'))

const TheaterIndex = lazy(() => import('./pages/admin/TheaterIndex'))

function App() {

  return (
    <BrowserRouter>
      <Suspense fallback={
        <div></div>
      }>
        <Switch>
          <PrivateRoute exact path={RoutePath.DASHBOARD_ADMIN} component={DashboardAdmin} />
          <PrivateRoute exact path={RoutePath.DASHBOARD_CUSTOMER} component={DashboardCustomer} />
          <PrivateRoute path={RoutePath.CUSTOMER} component={Customer} />
          <PrivateRoute path={RoutePath.TICKET} component={Ticket} />
          <PrivateRoute path={RoutePath.TICKET_DETAIL} component={TicketDetail} />
          <PrivateRoute path={RoutePath.TICKET_CUSTOMER} component={TicketCustomer} />
          <PrivateRoute path={RoutePath.CUSTOMER_ORDER} component={CustomerOrder} />
          <PrivateRoute path={RoutePath.THEATER_INDEX} component={TheaterIndex} />

          <PublicRoute path={RoutePath.THEATERS} component={Theaters} />
          <PublicRoute path={RoutePath.THEATER_DETAIL} component={TheaterDetail} />
          <PublicRoute path={RoutePath.TICKET_HOME} component={TicketHome} />
          <PublicRoute path={RoutePath.TICKET_CUSTOMER_DETAIL} component={TicketCustomerDetail} />
          <PublicRoute path={RoutePath.LOGIN} component={Login} />
          <PublicRoute path={RoutePath.REGISTER} component={Register} />
          <PublicRoute exact path={RoutePath.DASHBOARD} component={Dashboard} />
        </Switch>
      </Suspense>
    </BrowserRouter>
  );
}

// function App() {
//   const [token, setToken] = useState();

//   if(!token) {
//     return <Login setToken={setToken} />
//   }

//   return (
//     <div className="wrapper">
//       <h1>Application</h1>
//       <BrowserRouter>
//         <Switch>
//           <Route exact path={RoutePath.DASHBOARD} component={Dashboard} />
//         </Switch>
//       </BrowserRouter>
//     </div>
//   );
// }

export default App;
