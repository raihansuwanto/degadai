import React, { Component } from 'react';
import './Footer.css';

class Footer extends Component {
    render() {
        return (
            <>
                <footer id="footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-7">
                                <ul class="footer-left-menu">
                                    <li><a href="#">Profile</a></li>
                                    <li><a href="#">Terms of Use</a></li>
                                    <li><a href="#">Advertising</a></li>
                                </ul>
                            </div>

                            <div class="col-5 text-right">
                                <ul class="footer-right-menu">
                                    <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                    <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#" target="_blank"><i class="fab fa-youtube"></i></a></li>
                                </ul>
                            </div>
                        </div>

                        <div id="copyrights">
                        </div> 
                    </div>
                </footer>
            </>
        )
    }
}

export default Footer