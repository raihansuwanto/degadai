const RoutePath = {
    DASHBOARD: "/",
    DASHBOARD_ADMIN: "/dashboard/admin",
    DASHBOARD_CUSTOMER: "/dashboard/customer",
    LOGIN: "/login",
    REGISTER: "/register",
    CUSTOMER: "/customer",
    CUSTOMER_ADD: "/customer/add",
    CUSTOMER_SEARCH: "/customer/search",
    CUSTOMER_ORDER: "/customer/order",
    TICKET: "/ticket",
    TICKET_ADD: "/ticket/add",
    TICKET_SEARCH: "/ticket/search",
    TICKET_INDEX: "/ticket/index",
    TICKET_DETAIL: "/ticket-detail/:id",
    TICKET_CUSTOMER: "/ticket-customer",
    TICKET_CUSTOMER_INDEX: "/ticket-customer/index",
    TICKET_CUSTOMER_ORDER: "/ticket-customer/order",
    TICKET_CUSTOMER_DETAIL: "/ticket-customer/detail/:id",
    TICKET_HOME: "/ticket-home",
    THEATER_INDEX: "/theater-index",
    THEATERS: "/theaters",
    THEATER_DETAIL: "/theater-detail/:id",
    DEFAULT_IMAGE: "/default.png"
}

export default RoutePath;