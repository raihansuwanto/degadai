import { Component } from "react";
import { Link } from "react-router-dom";
import { Alert, Button, Col, Container, Row } from "react-bootstrap";
import RoutePath from "../../RoutePath";

class Customer extends Component {
    render() {
        return (
            <>
                <Alert variant="primary" className="text-center">Customer Landing Page</Alert>
                <Container>
                    <Row>
                        <Col md={12}>
                            <Link to={RoutePath.DASHBOARD}>
                                <Button variant="warning">Back</Button>
                            </Link>
                        </Col>
                        <Col md={12} className="mt-5">
                            <Link to={RoutePath.CUSTOMER_ADD}>
                                <Button variant="success">Add new customer</Button>
                            </Link>
                        </Col>
                        <Col md={12} className="mt-5">
                            <Link to={RoutePath.CUSTOMER_SEARCH}>
                                <Button variant="primary">Search customer</Button>
                            </Link>
                        </Col>
                    </Row>
                </Container>
            </>
        )
    }
}

export default Customer;