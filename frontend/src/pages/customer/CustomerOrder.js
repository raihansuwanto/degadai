import axios from "axios";
import { Component } from "react";
import { Alert, Breadcrumb, Button, Col, Container, Form, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import RoutePath from "../../RoutePath";
import { getToken } from '../../Utils/Common';
import { useTable } from "react-table";
import React, { useState, useEffect, useMemo, useRef } from "react";
import { ReactDOM,unstable_renderSubtreeIntoContainer } from "react-dom";
import Navbar from "../../Navbar/Navbar"
import "./Customer.css"

const CustomerOrder = props => {
    const initialTicketState = {
      id: null,
      name: "",
      description: "",
      stock: "",
      image: ""
    };
// }

const token = getToken();
const [ticket, setTicket] = useState([]);
const [searchNama, setSearchName] = useState("");
const ticketRef = useRef();

// const getTicket = id => {
//     axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
//     axios.get(`${process.env.REACT_APP_BASE_API_URL}ticket/${id}`, {
//         name: this.state.search_query
//     })
//     .then(response => {
//         setCurrentTicket(response.data.data);
//         console.log(response.data.data);
//     })
//     .catch(error => {
//         console.log(error)
//     });
//   };

useEffect(() => {
  retrieveTicket();
}, []);

  const retrieveTicket = () => {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
    axios.get(`${process.env.REACT_APP_BASE_API_URL}order`, {
    })
    .then(response => {
        setTicket(response.data.data);
        console.log(response.data.data);
    })
    .catch(error => {
        console.log(error)
    });
  };

  const orderTicket = (rowIndex) => {
    console.log(rowIndex);
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
    axios.post(`${process.env.REACT_APP_BASE_API_URL}ticket/${rowIndex}/order`, {
    })
    .then(response => {
        console.log(response.data.data);
        props.history.push(RoutePath.TICKET_CUSTOMER_INDEX);
      })
    .catch(error => {
        console.log(error)
    });  };

  const refreshList = () => {
    retrieveTicket();
  };

  const columns = useMemo(
    () => [
      {
        Header: "Customer",
        accessor: "user.name",
      },
      {
        Header: "Ticket",
        accessor: "ticket.name",
      },
      {
        Header: "Date",
        accessor: "created_at",
        Cell: (props) => {
          const rowData = props.row.original.created_at;
          return (
            rowData
          );
          console.log(rowData);
          // console.log(new Intl.DateTimeFormat('en-US', {year: 'numeric', month: '2-digit',day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit'}).format(rowData));
        }
      },

    ],
    []
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({
    columns,
    data: ticket,
  });

// class TicketIndex extends Component {

const findByName = e => {
    const searchTitle = e.target.value;
        axios.get(`${process.env.REACT_APP_BASE_API_URL}ticket?name=${searchNama}`, {
        }).then(response => {
            setTicket(response.data.data);
            console.log(response.data.data)
        }).catch(error => {
            console.log(error)
        })
    }

const onChangeSearchName = (e) => {
  const searchName = e.target.value;
  setSearchName(searchName);
};

    // render() {
        return (
          <>
          <Navbar />
          <br/>
          <br/>
          <Container>
            <Row class="text-center">
              <h3>
                Customer Order
              </h3>
            </Row>
            <Row>
              <div className="col-md-12 list">
                <table
                  className="table table-striped table-bordered"
                  {...getTableProps()}
                >
                  <thead className="head-table">
                    {headerGroups.map((headerGroup) => (
                      <tr {...headerGroup.getHeaderGroupProps()}>
                        {headerGroup.headers.map((column) => (
                          <th {...column.getHeaderProps()}>
                            {column.render("Header")}
                          </th>
                        ))}
                      </tr>
                    ))}
                  </thead>
                  <tbody {...getTableBodyProps()}>
                    {rows.map((row, i) => {
                      prepareRow(row);
                      return (
                        <tr {...row.getRowProps()}>
                          {row.cells.map((cell) => {
                            return (
                              <td className="cell-table" {...cell.getCellProps()}>{cell.render("Cell")}</td>
                            );
                          })}
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>    
            </Row>
          </Container>
          <div className="list row">
          
        </div>
        </>
          );
        }
    // }

export default CustomerOrder;