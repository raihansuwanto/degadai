import { Component } from "react";
import { Route } from "react-router";
import Customer from "./Customer";
import CustomerAdd from "./CustomerAdd";
import CustomerSearch from "./CustomerSearch";
import CustomerOrder from "./CustomerOrder";
import RoutePath from "../../RoutePath";

class CustomerRoute extends Component {
    render() {
        return (
            <>
                <Route exact path={this.props.match.path} component={Customer} />
                <Route path={RoutePath.CUSTOMER_ADD} component={CustomerAdd} />
                <Route path={RoutePath.CUSTOMER_SEARCH} component={CustomerSearch} />
                <Route path={RoutePath.CUSTOMER_ORDER} component={CustomerOrder} />
            </>
        )
    }
}

export default CustomerRoute;