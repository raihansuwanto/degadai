import { Component } from "react";
import { Route } from "react-router";
import Ticket from "./Ticket";
import TicketAdd from "./TicketAdd";
import TicketIndex from "./TicketIndex";
import TicketSearch from "./TicketSearch";
import TicketDetail from "./TicketDetail";
import RoutePath from "../../RoutePath";

class TicketRoute extends Component {
    render() {
        return (
            <>
                <Route exact path={this.props.match.path} component={Ticket} />
                <Route path={RoutePath.TICKET_ADD} component={TicketAdd} />
                <Route path={RoutePath.TICKET_SEARCH} component={TicketSearch} />
                <Route path={RoutePath.TICKET_INDEX} component={TicketIndex} />
                <Route path={RoutePath.TICKET_DETAIL} component={TicketDetail} />
            </>
        )
    }
}

export default TicketRoute;