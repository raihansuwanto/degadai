import axios from "axios";
import { Component } from "react";
import { Alert, Button, Container, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import RoutePath from "../../RoutePath";
import { getToken } from '../../Utils/Common';
import Navbar from "../../Navbar/Navbar"

const token = getToken();
class TicketAdd extends Component {
    constructor() {
        super();

        this.state = {
            name: '',
            description: '',
            stock: '',
            image:null
        }
    }

    add = (event) => {
        event.preventDefault()

        let data = {
            name: this.state.name,
            description: this.state.description,
            stock: parseInt(this.state.stock),
            image: this.state.image
        }

        if (
            !this.state.name ||
            !this.state.description ||
            !this.state.stock
            ) alert("mohon isi lengkap form")

        const formData = new FormData();
        formData.append('image',this.state.image)
        console.log(formData);
    
        axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
        axios.post(`${process.env.REACT_APP_BASE_API_URL}ticket`, formData,{
            headers: {
                'Content-Type': `false`
            }
        })
        .then(response => {
            alert("data berhasil ditambahkan")
            this.setState({
                name: '',
                description: '',
                stock: '',
                image:''
            })
            event.history.push("/ticket/index");
        }).catch(error => {
            console.log(error)
        })

    }

    render() {
        return (
            <>
                <Alert variant="primary" className="text-center">Ticket Add</Alert>
                <Link to={RoutePath.TICKET_INDEX}>
                    <Button variant="warning">Back</Button>
                </Link>
                <Container className="mt-4">
                    <Form 
                        onSubmit={this.add}
                        enctype="multipart/form-data"
                        >
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter your Ticket name"
                                value={this.state.name}
                                onChange={(event) => this.setState({name: event.target.value})}
                                required
                            />
                        </Form.Group>
                        <Form.Group controlId="formBasicDescription">
                            <Form.Label>Description</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Description" 
                                value={this.state.description}
                                onChange={(event) => this.setState({description: event.target.value})}
                                required
                            />
                        </Form.Group>
                        <Form.Group controlId="formBasicStock">
                            <Form.Label>Stock</Form.Label>
                            <Form.Control
                                type="number"
                                placeholder="Enter Stock" 
                                value={this.state.stock}
                                onChange={(event) => this.setState({stock: event.target.value})}
                                required
                            />
                        </Form.Group>
                        <Form.Group controlId="formBasicImage">
                            <Form.Label>Image</Form.Label>
                            <Form.File
                                type="file"
                                className="dropify"
                                id="image"
                                name="image"
                                value={this.state.image}
                                onChange={(event) => this.setState({image: event.target.value})}
                            />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Form>
                </Container>
            </>
        );
    }
}

export default TicketAdd;