import axios from "axios";
import { Component } from "react";
import { Alert, Button, Col, Container, Form, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import RoutePath from "../../RoutePath";
import { getToken } from '../../Utils/Common';

const token = getToken();

class TicketSearch extends Component {
    constructor() {
        super();

        this.state = {
            search_query: '',
            show_result_word: '',
            search_result: [],
        }
    }

    searchByName = (event) => {
        event.preventDefault()
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
        axios.get(`${process.env.REACT_APP_BASE_API_URL}ticket`)
        .then(response => {
            this.setState({
                search_result: response.data.data,
                show_result_word: this.state.search_query,
            })
            console.log(response.data.data)
        }).catch(error => {
            console.log(error)
        })
    }

    render() {
        return (
            <>
                <Alert variant="primary" className="text-center">Customer Search</Alert>
                <Link to={RoutePath.TICKET}>
                    <Button variant="warning">Back</Button>
                </Link>
                <Container className="mt-4">
                    <Form onSubmit={this.searchByName}>
                        <Form.Group>
                            <Form.Control
                                type="text"
                                placeholder="Type a customer name"
                                value={this.state.search_query}
                                onChange={(event) => this.setState({search_query: event.target.value})}
                            />
                        </Form.Group>
                        <Button variant="primary" type="submit">Search</Button>
                    </Form>

                    {
                        this.state.show_result_word &&
                        <Alert variant="secondary" className="mt-5">Showing search result for "{this.state.show_result_word}"</Alert>
                    }

                    {
                        this.state.search_result.map((value, index) => (
                            <div key={index}>
                                <Row className="mt-4">
                                    <Col>
                                        Name
                                    </Col>
                                    <Col>
                                        {value.name}
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        Email
                                    </Col>
                                    <Col>
                                        {value.email}
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        Address
                                    </Col>
                                    <Col>
                                        {value.address}
                                    </Col>
                                </Row>
                            </div>
                        ))
                    }
                </Container>
            </>
        )
    }
}

export default TicketSearch;