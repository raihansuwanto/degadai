import axios from "axios";
import { getToken } from '../../Utils/Common';
import { useTable } from "react-table";
import React, { useState, useEffect, useMemo, useRef } from "react";
import "./Ticket.css"
import Template from "../../components/template/Template";
import { Ticket } from '../../components/container/ticket'

const TicketIndex = props => {
    const initialTicketState = {
      id: null,
      name: "",
      description: "",
      stock: "",
      image: ""
    };
// }

const token = getToken();
const [ticket, setTicket] = useState([]);
const [searchNama, setSearchName] = useState("");

const onSubmit = (event) => {
  
  console.log(event.target.name.value);
  let data = {
    name: event.target.name.value,
    description: event.target.description.value,
    stock: parseInt(event.target.stock.value) 
  }

  axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  axios.post(`${process.env.REACT_APP_BASE_API_URL}ticket`, data)
  .then(response => {
      alert("data berhasil ditambahkan")
      event.history.push("/ticket/index");
  }).catch(error => {
      console.log(error)
  })
}

useEffect(() => {
  retrieveTicket();
}, []);

  const retrieveTicket = () => {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
    axios.get(`${process.env.REACT_APP_BASE_API_URL}ticket`, {
    })
    .then(response => {
        setTicket(response.data.data);
        console.log(response.data.data);
    })
    .catch(error => {
        console.log(error)
    });
  };

  const openTicket = (rowIndex) => {
    props.history.push("/ticket-detail/" + rowIndex);
  };

  const refreshList = () => {
    retrieveTicket();
  };

  const deleteTicket = (rowIndex) => {

    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
    axios.delete(`${process.env.REACT_APP_BASE_API_URL}ticket/${rowIndex}`, {
    })
    .then(response => {
        console.log(response.data.data);
        refreshList();
      })
    .catch(error => {
        console.log(error)
    });
  };

  const columns = useMemo(
    () => [
      {
        Header: "#",
        accessor: (row, index) => index + 1,
      },
      {
        Header: "Nama",
        accessor: "name",
      },
      {
        Header: "Description",
        accessor: "description",
      },
      {
        Header: "Stock",
        accessor: "stock",
      },
      {
        Header: "Ordered Ticket",
        accessor: "ordered_stock",
      },
      {
        Header: "Available Ticket",
        accessor: "available_ticket",
        Cell: (props) => {
          const rowData = props.row.original;
          return (
            rowData.stock - rowData.ordered_stock
          );
        }
      },
      {
        Header: "Actions",
        accessor: "actions",
        Cell: (props) => {
          const rowIdx = props.row.original.ID;
          return (
            <div>
              <span onClick={() => openTicket(rowIdx)}>
                <i className={'fas fa-eye'}></i>
              </span>
              <span onClick={() => deleteTicket(rowIdx)}>
                <i className={'fas fa-trash-alt'}></i>
              </span>
            </div>
          );
        },
      },
    ],
    []
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({
    columns,
    data: ticket,
  });

// class TicketIndex extends Component {

const findByName = e => {
    const searchTitle = e.target.value;
        axios.get(`${process.env.REACT_APP_BASE_API_URL}ticket?name=${searchNama}`, {
        }).then(response => {
            setTicket(response.data.data);
            console.log(response.data.data)
        }).catch(error => {
            console.log(error)
        })
    }

const onChangeSearchName = (e) => {
  const searchName = e.target.value;
  setSearchName(searchName);
};

    // render() {
        return (
          <>
          <Template>
          <br/>
          <br/>
          <div className="list-row">
          <div className="col-md-8">
            <div className="input-group mb-3">
              <input
                type="text"
                className="form-control"
                placeholder="Search by title"
                value={searchNama}
                onChange={onChangeSearchName}
              />
              <div className="input-group-append">
                <button
                  className="btn btn-secondary"
                  type="button"
                  onClick={findByName}
                >
                  Search
                </button>
              </div>
            </div>
          </div>
          <div className="col-md-12 list">
            <table
              className="table table-striped table-bordered"
              {...getTableProps()}
            >
              <thead className="head-table">
                {headerGroups.map((headerGroup) => (
                  <tr {...headerGroup.getHeaderGroupProps()}>
                    {headerGroup.headers.map((column) => (
                      <th {...column.getHeaderProps()}>
                        {column.render("Header")}
                      </th>
                    ))}
                  </tr>
                ))}
              </thead>
              <tbody {...getTableBodyProps()}>
                {rows.map((row, i) => {
                  prepareRow(row);
                  return (
                    <tr {...row.getRowProps()}>
                      {row.cells.map((cell) => {
                        return (
                          <td className="cell-table" {...cell.getCellProps()}>{cell.render("Cell")}</td>
                        );
                      })}
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <Ticket modalId="2" triggerText="Add Ticket" onSubmit={onSubmit} />
          </div>
        </div>
        </Template>
        </>
          );
        }
    // }

export default TicketIndex;