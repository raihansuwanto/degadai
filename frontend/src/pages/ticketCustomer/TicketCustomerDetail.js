import axios from "axios";
import { Component } from "react";
import { Alert, Figure, Button, Col, Container, Form, Row, FormGroup } from "react-bootstrap";
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import RoutePath from "../../RoutePath";
import { getToken } from '../../Utils/Common';
import { useTable } from "react-table";
import Navbar from "../../Navbar/Navbar";
import Footer from "../../Footer/Footer";
import "./TicketCustomerDetail.css"
import FormFileInput from "react-bootstrap/esm/FormFileInput";

const TicketCustomerDetail = props => {
    const initialTicketState = {
      id: null,
      name: "",
      description: "",
      stock: "",
      image: null
    };
// }

const token = getToken();
const [currentTicket, setCurrentTicket] = useState(initialTicketState);
const [message, setMessage] = useState("");
const image = currentTicket.image
              ?`${process.env.REACT_APP_IMAGE_URL}`+ currentTicket.image
              : RoutePath.DEFAULT_IMAGE;

const [file, setFile] = useState();
const [uploadedFile, setUploadedFile] = useState();

useEffect(() => {
  getTicket(props.match.params.id);
}, [props.match.params.id]);

const handleInputChange = event => {
  const { name, value } = event.target;
  setCurrentTicket({ ...currentTicket, [name]: value });
};

const getTicket = id => {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
    axios.get(`${process.env.REACT_APP_BASE_API_URL}ticket/${id}`, {
    })
    .then(response => {
        setCurrentTicket(response.data.data);
        console.log(response.data.data);
    })
    .catch(error => {
        console.log(error)
    });
  };

  const updateTicket = e=> {
    e.preventDefault();
    // console.log(file);
    const formData = new FormData();
    formData.append('image',file);    
    console.log(formData)
    
    // let data = {
    //   name: currentTicket.name,
    //   description: currentTicket.description,
    //   stock: currentTicket.stock,
    //   // image: this.state.image
    // }

    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
    axios.put(`${process.env.REACT_APP_BASE_API_URL}ticket/${currentTicket.ID}`, {
      formData
      // name: currentTicket.name,
      // description: currentTicket.description,
      // stock: currentTicket.stock
      // image
    })
      .then(response => {
        console.log(response.data);
        // setMessage("The tutorial was updated successfully!");
      })
      .catch(e => {
        console.log(e);
      });
  };

  const handleImageChange = (e) => {
    e.preventDefault();
    let reader = new FileReader();
    let thefile = e.target.files[0];
    reader.onloadend = () => {
      setFile(thefile)
      setUploadedFile(reader.result)
    }
    reader.readAsDataURL(thefile)
  }


    // render() {
        return (
          <>
          <div className="wrapper">
          <Navbar />
            <section className="content">
              <div className="contents-wrap">
                <Container>
                  <div className="row">
                    <div className="col-12 col-padding">
                      <div className="single-title">
                        <h3>NOW PLAYING</h3>
                      </div>
                    </div>
                  </div>
                  <div className="main-content">
                    <div className="row">
                      <div className="col-4 col-padding">
                        <div className="poster-box">
                          <img className="ticket-poster"
                            src={
                                  currentTicket.image
                                  ? `${process.env.REACT_APP_IMAGE_URL}` + currentTicket.image
                                  : RoutePath.DEFAULT_IMAGE 
                                }
                          />
                          <a className="buy-ticket" href={RoutePath.CUSTOMER_ORDER}>Buy Ticket</a>
                        </div>
                      </div>
                      <div className="col-8 col-padding">
                        <div className="desc-box">
                          <h2 className="title-ticket">{currentTicket.name}</h2>
                          <ul className="desc-ticket">
                            <li className="band-genre">
                              <span>Genre</span>
                              : {currentTicket.genre}
                            </li><br/>
                            <li className="band-vokalist">
                              <span>Vokalis</span>
                              : {currentTicket.vokalist}
                            </li><br/>
                            <li className="band-member">
                              <span>Member</span>
                              : {currentTicket.member}
                            </li><br/>
                          </ul>
                        </div>
                        <div class="text-center">
                        <div class="desc-links">
                          <div className="links">
                            <a href="#watchTrailer" data-lightbox="inline">Watch Trailer</a>
                          </div>
                          <div className="links">
                            <a data-toggle="collapse" href="#scheduleCollapse" role="button" aria-expanded="false" aria-controls="scheduleCollapse">Playing At</a>                            
                          </div>
                        </div>
                      </div>
                        <div className="desc-band">
                          <h4 className="desc">DESCRIPTION</h4>
                          <p>{currentTicket.description}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                </Container>
              </div>
            </section>
          <Footer />
          </div>
          </>
          );
        }
    // }

export default TicketCustomerDetail;