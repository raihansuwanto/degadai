import axios from "axios";
import { Alert, Button, Col, Container, Form, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import RoutePath from "../../RoutePath";
import { getToken, getUser } from '../../Utils/Common';
import { useTable } from "react-table";
import React, { useState, useEffect, useMemo, useRef } from "react";
import Navbar from "../../Navbar/Navbar"
import "./TicketCustomer.css"


const TicketCustomerIndex = props => {
    const initialTicketState = {
      id: null,
      name: "",
      description: "",
      stock: ""
    };
// }

const token = getToken();
const user = getUser();
const [order, setOrder] = useState([]);
const [searchNama, setSearchName] = useState("");
const ticketRef = useRef();

// const getTicket = id => {
//     axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
//     axios.get(`${process.env.REACT_APP_BASE_API_URL}ticket/${id}`, {
//         name: this.state.search_query
//     })
//     .then(response => {
//         setCurrentTicket(response.data.data);
//         console.log(response.data.data);
//     })
//     .catch(error => {
//         console.log(error)
//     });
//   };

useEffect(() => {
  retrieveTicket();
}, []);

  const retrieveTicket = () => {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
    axios.get(`${process.env.REACT_APP_BASE_API_URL}order?user_id=` + user.user_id, {
    })
    .then(response => {
        setOrder(response.data.data);
        console.log(response.data.data);
    })
    .catch(error => {
        console.log(error)
    });
  };

  const openTicket = (rowIndex) => {
    props.history.push("/ticket/" + rowIndex);
  };

  const refreshList = () => {
    retrieveTicket();
  };

  const columns = useMemo(
    () => [
      {
        Header: "Nama",
        accessor: "ticket.name",
      },
      {
        Header: "Description",
        accessor: "ticket.description",
      },
      {
        Header: "Date",
        accessor: "created_at",
      },
      // {
      //   Header: "Actions",
      //   accessor: "actions",
      //   Cell: (props) => {
      //     const rowIdx = props.row.original.ID;
      //     return (
      //       <div>
      //         <span onClick={() => openTicket(rowIdx)}>
      //           <button
      //             type="open"
      //             className="badge badge-success"
      //           >
      //             Detail
      //           </button>
      //         </span>
      //       </div>
      //     );
      //   },
      // },
    ],
    []
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({
    columns,
    data: order,
  });

// class TicketIndex extends Component {

const findByName = e => {
    const searchTitle = e.target.value;
        axios.get(`${process.env.REACT_APP_BASE_API_URL}ticket?name=${searchNama}`, {
        }).then(response => {
            setOrder(response.data.data);
            console.log(response.data.data)
        }).catch(error => {
            console.log(error)
        })
    }

const onChangeSearchName = (e) => {
  const searchName = e.target.value;
  setSearchName(searchName);
};

    // render() {
        return (
          <>
          <Navbar />
          <br/>
          <br/>
          <Container>
            <Row>
            <div className="col-md-8">
              <div className="input-group mb-3">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Search by title"
                  value={searchNama}
                  onChange={onChangeSearchName}
                />
                <div className="input-group-append">
                  <button
                    className="btn btn-secondary"
                    type="button"
                    onClick={findByName}
                  >
                    Search
                  </button>
                </div>
              </div>
            </div>
            </Row>
            <Row>
              <div className="col-md-12 list">
                <table
                  className="table table-striped table-bordered"
                  {...getTableProps()}
                >
                  <thead>
                    {headerGroups.map((headerGroup) => (
                      <tr {...headerGroup.getHeaderGroupProps()}>
                        {headerGroup.headers.map((column) => (
                          <th {...column.getHeaderProps()}>
                            {column.render("Header")}
                          </th>
                        ))}
                      </tr>
                    ))}
                  </thead>
                  <tbody {...getTableBodyProps()}>
                    {rows.map((row, i) => {
                      prepareRow(row);
                      return (
                        <tr {...row.getRowProps()}>
                          {row.cells.map((cell) => {
                            return (
                              <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                            );
                          })}
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </Row>
            <Row>
              <Col md={12} className="mt-5">
                <Link to={RoutePath.TICKET_CUSTOMER_ORDER}>
                    <Button variant="success">Add new Order</Button>
                </Link>
              </Col>
            </Row>
          </Container>
        </>
          );
        }
    // }

export default TicketCustomerIndex;