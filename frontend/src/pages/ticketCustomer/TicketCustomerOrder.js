import axios from "axios";
import { Component } from "react";
import { Alert, Button, Card, CardColumns, CardDeck, CardGroup, Container, Row} from "react-bootstrap";
import { Link } from "react-router-dom";
import RoutePath from "../../RoutePath";
import { getToken } from '../../Utils/Common';
import { useTable } from "react-table";
import React, { useState, useEffect, useMemo, useRef } from "react";
import Navbar from "../../Navbar/Navbar"
import Footer from "../../Footer/Footer"
import { Slide } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'

const CustomerOrder = props => {
    const initialTicketState = {
      id: null,
      name: "",
      description: "",
      stock: "",
      image: ""
    };
// }

const token = getToken();
const [areaId, setAreaId] = useState(1);
const [ticket, setTicket] = useState([]);
const [ticketComingSoon, setTicketComingSoon] = useState([]);
const [images, setImages] = useState([]);
const [areas, setAreas] = useState([]);
const [searchNama, setSearchName] = useState("");
const ticketRef = useRef();

useEffect(() => {
  console.log(areaId);
  retrieveTicket();
  retrieveImages();
  retrieveAreas();
  retrieveTicketCS();
}, []);

  const retrieveTicket = () => {
    axios.get(`${process.env.REACT_APP_BASE_API_URL}ticket-public?now_playing=true&area_id=1`, {
    })
    .then(response => {
        setTicket(response.data.data);
        console.log(response.data.data);
    })
    .catch(error => {
        console.log(error)
    });
  };

  const retrieveTicketCS = () => {
    axios.get(`${process.env.REACT_APP_BASE_API_URL}ticket-public?cooming_soon=true`, {
    })
    .then(response => {
        setTicketComingSoon(response.data.data);
        console.log(response.data.data);
    })
    .catch(error => {
        console.log(error)
    });
  };

  const retrieveImages = () => {
    axios.get(`${process.env.REACT_APP_BASE_API_URL}ticket-images`, {
    })
    .then(response => {
        setImages(response.data.data);
        console.log(response.data.data);
    })
    .catch(error => {
        console.log(error)
    });
  };

  const retrieveAreas = () => {
    axios.get(`${process.env.REACT_APP_BASE_API_URL}area`, {
      })
      .then(response => {
          setAreas(response.data.data);
          console.log(response.data.data);
      })
      .catch(error => {
          console.log(error)
      });
    };

  const handleAreaChange = e => {
    const area = e.target.value;
    setAreaId(area);

    axios.get(`${process.env.REACT_APP_BASE_API_URL}ticket-public?area_id=${area}&now_playing=true`, {
    })
    .then(response => {
        setTicket(response.data.data);
        console.log(response.data.data);
    })
    .catch(error => {
        console.log(error)
    });

  }

const slideShow = images.map((img) =>
      // <div>
          <div className="each-slide">
            <div class="crop-height">
              <img className="slide-show"
                src={`${process.env.REACT_APP_IMAGE_URL}` + img.image}
              />
            </div>
          </div>
      // </div>
  );

  const orderTicket = (rowIndex) => {
    console.log(rowIndex);
    // axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
    axios.post(`${process.env.REACT_APP_BASE_API_URL}ticket/${rowIndex}/order`, {
    })
    .then(response => {
        console.log(response.data.data);
        props.history.push(RoutePath.TICKET_CUSTOMER_INDEX);
      })
    .catch(error => {
        console.log(error)
    });  };

  const refreshList = () => {
    retrieveTicket();
  };

  const columns = useMemo(
    () => [
      {
        Header: "Nama",
        accessor: "name",
      },
      {
        Header: "Description",
        accessor: "description",
      },
      {
        Header: "Stock",
        accessor: "stock",
      },
      {
        Header: "Ordered Ticket",
        accessor: "ordered_stock",
      },
      {
        Header: "Available Ticket",
        accessor: "available_ticket",
        Cell: (props) => {
          const rowData = props.row.original;
          return (
            rowData.stock - rowData.ordered_stock
          );
        }
      },
      {
        Header: "Image",
        // Cell: (props) => {
        //   const rowData = props.row.original;

        //   if(rowData.image != null){
        //     const image = `${process.env.REACT_APP_IMAGE_URL}`+ rowData.image+"auto=compress&cs=tinysrgb&h=350";
        //     return (
        //       <feImage
        //       src={image}
        //       />
        //     );
        //   }
        // }
      },
      {
        Header: "Actions",
        accessor: "actions",
        Cell: (props) => {
          const rowIdx = props.row.original.ID;
          return (
            <div>
              <span onClick={() => orderTicket(rowIdx)}>
                <button
                  type="open"
                  className="badge badge-success"
                >
                  Order
                </button>
              </span>
            </div>
          );
        },
      },
    ],
    []
  );

  const cards = ticket.map((post) =>
    
    <div className="col-3">
      <div className="movie">
        <a href={"/ticket-customer/detail/"+post.ID}>
          <div className="ticket-poster">
            <img className="image-list"
              src={
                    post.image
                    ? `${process.env.REACT_APP_IMAGE_URL}` + post.image
                    : RoutePath.DEFAULT_IMAGE 
                  }
            />
          </div>
          <div className="ticket-desc">
              <h4>{post.name}</h4>  
            </div>
        </a>
      </div>
    </div>
  );

  const coomingSoonTicket = ticketComingSoon.map((post) =>
    
    <div className="col-3">
      <div className="movie">
        <a href={"/ticket-customer/detail/"+post.ID}>
          <div className="ticket-poster">
            <img className="image-list"
              src={
                    post.image
                    ? `${process.env.REACT_APP_IMAGE_URL}` + post.image
                    : RoutePath.DEFAULT_IMAGE 
                  }
            />
          </div>
          <div className="ticket-desc">
              <h4>{post.name}</h4>  
            </div>
        </a>
      </div>
    </div>
  );

  const areaMap = areas.map((area) =>
    <option value={area.ID}>{area.name}</option>
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({
    columns,
    data: ticket,
  });

// class TicketIndex extends Component {

const findByName = e => {
    const searchTitle = e.target.value;
        axios.get(`${process.env.REACT_APP_BASE_API_URL}ticket?name=${searchNama}`, {
        }).then(response => {
            setTicket(response.data.data);
            console.log(response.data.data)
        }).catch(error => {
            console.log(error)
        })
    }

const onChangeSearchName = (e) => {
  const searchName = e.target.value;
  setSearchName(searchName);
};

    // render() {
        return (
          <>
          <Navbar />
          <Container>
            <div className="content-wrap">
              <div className="slide-container">
              <Slide>
                {slideShow}
              </Slide>
            </div>
            <section id="now-playing">
            <div id="now-playing-section" className="section">
              <div class="heading-twenty">
                <h3>Now Playing</h3>
                <div class="select-twenty">
                  <select class="custom-select" id="cityChanged" onChange={handleAreaChange}>
                    {areaMap}
                  </select>
                </div>
              </div>
              <div id="list-ticket" className=" row topmargin clearfix">
                {cards}
              </div>              
            </div>
            </section>

            <section id="upcoming">
              <div id="upcoming-section" className="section">
                <div class="heading-twenty">
                  <h3>Upcoming</h3>
                </div>
                <div id="list-ticket" className=" row topmargin clearfix">
                  {coomingSoonTicket}
                </div>              
              </div>
            </section>

            </div>
            </Container>
            <Footer />
        </>
          );
        }

export default CustomerOrder;