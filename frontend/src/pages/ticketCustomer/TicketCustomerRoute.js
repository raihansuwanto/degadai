import { Component } from "react";
import { Route } from "react-router";
// import TicketCustomer from "./TicketCustomer";
import TicketCustomerOrder from "./TicketCustomerOrder";
import TicketCustomerIndex from "./TicketCustomerIndex";
import TicketCustomerDetail from "./TicketCustomerDetail";
import RoutePath from "../../RoutePath";
import Navbar from "../../Navbar/Navbar"

class TicketCustomerRoute extends Component {
    render() {
        return (
            <>
                <Route exact path={this.props.match.path} component={TicketCustomerIndex} />
                <Route path={RoutePath.TICKET_CUSTOMER_INDEX} component={TicketCustomerIndex} />
                <Route path={RoutePath.TICKET_CUSTOMER_ORDER} component={TicketCustomerOrder} />
                <Route path={RoutePath.TICKET_CUSTOMER_DETAIL} component={TicketCustomerDetail} />
            </>
        )
    }
}

export default TicketCustomerRoute;