import { Component } from "react";
import { Link, BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import RoutePath from "../RoutePath";
import { Alert, Button, Col, Container, Row } from "react-bootstrap";
import ReactDOM from "react-dom";
import {     removeUserSession,getToken,getUser } from '../Utils/Common';

const user = getUser();
const token = getToken();
const handleLogout = () => {
    removeUserSession();
  }
class Dashboard extends Component {
    render() {
        if(!token) {
            return (
                <Redirect to={RoutePath.TICKET_HOME}/>
            )
        }else{
            if(user.is_admin){
                return (
                    <Redirect to={RoutePath.TICKET_INDEX}/>
                )
            }else{
                return (
                    <Redirect to={RoutePath.TICKET_CUSTOMER_ORDER}/>
                )
            }
        }
    }
}

export default Dashboard;