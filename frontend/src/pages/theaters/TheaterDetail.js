import axios from "axios";
import { Component } from "react";
import { Alert, Figure, Button, Col, Container, Form, Row, FormGroup } from "react-bootstrap";
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import RoutePath from "../../RoutePath";
import { getToken } from '../../Utils/Common';
import { useTable } from "react-table";
import Navbar from "../../Navbar/Navbar";
import Footer from "../../Footer/Footer";
import "./TheaterDetail.css";
import Moment from 'react-moment';
import moment from "moment";


const TicketCustomerDetail = props => {
    const initialTheaterState = {
      id: null,
      name: "",
      description: "",
      address: "",
    };
// }
const [currentTheater, setCurrentTheater] = useState(initialTheaterState);
const [ticket, setTicket] = useState([]);
const [date, setDate] = useState('');
const image = currentTheater.image
              ?`${process.env.REACT_APP_IMAGE_URL}`+ currentTheater.image
              : RoutePath.DEFAULT_IMAGE;
var today = new Date();
const week = [
  moment(today).add(0, 'days').format("YYYY-MM-DD"),
  moment(today).add(1, 'days').format("YYYY-MM-DD"),
  moment(today).add(2, 'days').format("YYYY-MM-DD"),
  moment(today).add(3, 'days').format("YYYY-MM-DD"),
  moment(today).add(4, 'days').format("YYYY-MM-DD"),
  moment(today).add(5, 'days').format("YYYY-MM-DD")
]

useEffect(() => {
  setDate(moment(today).format("YYYY-MM-DD"));
  getTheater(props.match.params.id);
  getTicket(props.match.params.id,moment(today).format("YYYY-MM-DD"));
}, [props.match.params.id]);

  const getTheater = id => {
    axios.get(`${process.env.REACT_APP_BASE_API_URL}theater/${id}`, {
    })
    .then(response => {
        setCurrentTheater(response.data.data);
        console.log(response.data.data);
    })
    .catch(error => {
        console.log(error)
    });
  };

  const getTicket = (id,dateF) => {
    axios.get(`${process.env.REACT_APP_BASE_API_URL}theater/${id}/ticket?date=${dateF}`, {
    })
    .then(response => {
        setTicket(response.data.data);
        console.log(response.data.data);
    })
    .catch(error => {
        console.log(error)
    });
  };

  const handleInputChange = (date) => {
    setDate(date);
    getTicket(props.match.params.id,date);
  };

  const ticketMap = ticket.map((post)=>
            <div className="list-movie">
							<div class="row align-items-center">
								<div class="col-3">
									<div class="list-movie-poster">
											<img 
                      className="image-list"
                      src={
                        post.image
                        ? `${process.env.REACT_APP_IMAGE_URL}` + post.image
                        : RoutePath.DEFAULT_IMAGE 
                      }
                      alt="21CDMM"/>
											<br/>
									</div>
								</div>

								<div class="col-9">
									<div class="list-movie-detail">
										<h3>{post.name}</h3>

										<h5>SHOWTIME :</h5>

										<ul class="time-cinema theater">
                      {
                        post.shows.map((show)=>
                        <li class="active">
                          <a className="a-time" href="#" class="selectDate">
                            {
                              moment(moment(show.date).format("YYYY-MM-DD") + " " +show.theater_time.time_start).format("HH:mm")
                            
                            }
                          </a>
                        </li>
                        )
                      }
										</ul>
									</div>
								</div>
							</div>
						</div>
  );

    // render() {
        return (
          <>
          <div className="wrapper">
          <Navbar />
          <section className="page-title">
            <h1>{currentTheater.name}</h1>
          </section>
            <section className="content">
              <div className="contents-wrap">
                <div className="container-clear">
                  <div className="main-content main-detail">
                    <div class="row">
                      <div class="col-12">
                        <div class="cinema-gallery">
                          <img src="https://media.21cineplex.com/webcontent/gallery/pictures/144603614095379_430x280.jpg" class="img-fluid" alt="" width="925"/>
                        </div>
                      </div>
                    </div>
                    <div className="row topmargin-sm">
                      <div className="col-6">
                        <div className="address-cinema">
                          <div class="address-cinema-box">
                            <div class="address-cinema-block" id="address">
                              {currentTheater.address}						
                              <br/>
                            </div>
                            <div class="phone-cinema-block">
                            Phone: {currentTheater.telp}									
                            </div>
                            <div class="htm-cinema-block" id="divhtm">
                              {currentTheater.description}
                            </div>
                            <div class="feature-cinema clearfix">
                            <img src="https://21cineplex.com//theme/v5/assets/img/icons/theater/cafe.png" alt="Concessions / Cafe"/>                              <img src="https://21cineplex.com//theme/v5/assets/img/icons/theater/mtix.png" alt="MTIX"/>
                              <img src="https://21cineplex.com//theme/v5/assets/img/icons/theater/atmos.png" alt="Dolby Atmos"/>
                              <img src="https://21cineplex.com//theme/v5/assets/img/icons/theater/thx.png" alt="THX"/>								
                            </div>
                          </div>
                        </div>
                      </div>
                      
                    </div>
                    <div className="row topmargin-sm">
                      <div className="col-12">
                        <div className="schedule-wrap">
                          <div className="tabs theater-tabs">
                            <ul className="tab-nav clearfix ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
                              <li className="ui-state-default ui-corner-top ui-tabs-active ui-state-active" role="tab" tabindex="0" aria-controls="xxi-tab" aria-labelledby="xxitab" aria-selected="true" aria-expanded="true">
                                <a href="#xxi-tab" id="xxitab" className="ui-tabs-anchor" role="presentation" tabindex="-1">
                                  <img src="https://21cineplex.com//theme/v5/assets/img/logos/xxi-logo-table.png" alt="Cinema XXI"/>
                                </a>
                              </li>
                            </ul>
                            <div className="tab-container">
                              <div class="tab-content clearfix ui-tabs-panel ui-widget-content ui-corner-bottom" id="xxi-tab" aria-labelledby="xxitab" role="tabpanel" aria-hidden="false">
                                <table class="schedule-table">
                                  <thead>
                                    <tr>
                                      <th className="tg-uys7">
                                        SHOWTIME
                                        <ul class="nav nav-tabs schedule-pagination" id="myTab" role="tablist">
                                          <li className="nav-item"><a href="#" className="reguler_prevtab" data-reguler-prevdate="1"><i class="fas fa-angle-left"></i></a></li>

                                          {
                                            week.map((day) => 
                                            <li className="nav-item tanggalan-reguler" value={day} onClick={() => handleInputChange(day)}>
                                              <a className={ day == date?'tanggalan-aktiv':'tanggalan' } value={day} >
                                                {day}
                                              </a>
                                              </li>                                              
                                            )
                                          }
                                          <li className="nav-item"><a href="#" className="reguler_nexttab" data-reguler-nextdate="2" data-reguler-maxdate="5"><i class="fas fa-angle-right"></i></a></li>
                                          </ul>
                                      </th>
                                    </tr>
                                  </thead>
                                </table>
                              </div>
                            </div>
                            <div className="list-movies">
                              {ticketMap}
                            </div>
                            
                          </div>
                        </div>  
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          <Footer />
          </div>
          </>
          );
        }
    // }

export default TicketCustomerDetail;