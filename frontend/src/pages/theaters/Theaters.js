import { Component } from "react";
import Navbar from "../../Navbar/Navbar"
import Footer from "../../Footer/Footer"
import { Container, Table } from "react-bootstrap";
import "./Theaters.css"
import React, { useState, useEffect, useMemo, useRef } from "react";
import axios from "axios";


const Theaters = props => {

    const [theater, setTheater] = useState([]);
    const [areas, setAreas] = useState([]);
    const [areaId, setAreaId] = useState(1);

    const retrieveAreas = () => {
        axios.get(`${process.env.REACT_APP_BASE_API_URL}area`, {
          })
          .then(response => {
              setAreas(response.data.data);
              console.log(response.data.data);
          })
          .catch(error => {
              console.log(error)
          });
        };

    const retrieveTheater = () => {
        axios.get(`${process.env.REACT_APP_BASE_API_URL}theater?area_id=1`, {
        })
        .then(response => {
            setTheater(response.data.data);
            console.log(response.data.data);
        })
        .catch(error => {
            console.log(error)
        });
      };

      const handleAreaChange = e => {
        const area = e.target.value;
        setAreaId(area);
    
        axios.get(`${process.env.REACT_APP_BASE_API_URL}theater?area_id=${area}`, {
        })
        .then(response => {
            setTheater(response.data.data);
            console.log(response.data.data);
        })
        .catch(error => {
            console.log(error)
        });
    
      }

    // render() {
        
        useEffect(() => {
            retrieveTheater();
            retrieveAreas();
          }, []);

        return (
            <>
<div className="wrapper">
          <Navbar />
            <section className="content">
              <div className="content-wrap">
                <div className="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="heading-twenty bottommargin-sm">
                                <div class="select-twenty dark-twenty">
                                    <select class="custom-select" id="cityChanged" onChange={handleAreaChange}>
                                        {
                                            areas.map((area) =>
                                            <option value={area.ID}>{area.name}</option>
                                          )
                                        }
                                    </select>
                                </div>

                                <input type="text" name="cinema-search" id="cinemaSearch" placeholder="Search..."/>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div className="main-detail main-content">
                        <ul class="tab-nav clearfix ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
                            <li class="ui-state-default ui-corner-top ui-tabs-active ui-state-active" role="tab" tabindex="0" aria-controls="xxi-tab" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true">
                                <a href="#xxi-tab" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1">
                                    <img src="https://21cineplex.com//theme/v5/assets/img/logos/xxi-logo-table.png" alt="Cinema XXI"/>
                                </a>
                            </li>
                            
                            <li class="ui-state-default ui-corner-top ui-state-hover" role="tab" tabindex="-1" aria-controls="premiere-tab" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false">
                                <a href="#premiere-tab" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2">
                                    <img src="https://21cineplex.com//theme/v5/assets/img/logos/premiere-logo-table.png" alt="The Premiere"/>
                                </a>
                            </li>

                            <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="imax-tab" aria-labelledby="ui-id-3" aria-selected="false" aria-expanded="false">
                                <a href="#imax-tab" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-3">
                                    <img src="https://21cineplex.com//theme/v5/assets/img/logos/imax-logo-table.png" alt="IMAX"/>
                                </a>
                            </li>
                        </ul>
                        <div className="tab-container">
                        <Table hover>
                        <tbody>

                            {
                             
                             theater.map((post) =>
                                <tr>
                                <td>
                                    <a className="theater-link" href={`theater-detail/${post.ID}`}>
                                        {post.name}
                                    </a>
                                </td>
                                <td></td>
                                <td>{post.telp}</td>
                                </tr>
                             )
                             
                            }
                        </tbody>
                        </Table>
                        </div>
                    </div>
                </div>
              </div>
            </section>
          {/* <Footer /> */}
          </div>
            </>
        )
    }
// }

export default Theaters;