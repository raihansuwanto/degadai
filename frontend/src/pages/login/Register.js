import axios from "axios";
import { Component } from "react";
import { Alert, Button, Container, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import RoutePath from "../../RoutePath";
import { setUserSession } from '../../Utils/Common';

class Register extends Component {
    constructor() {
        super();

        this.state = {
            name: '',
            email: '',
            password: '',
            address: '',
        }
    }

    add = (event) => {
        event.preventDefault()

        let data = {
            name: this.state.name,
            email: this.state.email,
            password: this.state.password,
            address: this.state.address,
        }

        if (
            !this.state.name ||
            !this.state.email ||
            !this.state.password ||
            !this.state.address
            ) alert("mohon isi lengkap form")

        axios.post(`${process.env.REACT_APP_BASE_API_URL}auth/register`, data)
        .then(response => {
            alert("data berhasil ditambahkan")
            setUserSession(response.data.data.access_token, response.data.data);
            this.setState({
                name: '',
                email: '',
                password: '',
                address: '',
            })
            this.props.history.push(RoutePath.DASHBOARD_CUSTOMER);
        }).catch(error => {
            console.log(error)
        })

    }

    render() {
        return (
            <>
                <Alert variant="primary" className="text-center">Customer Add</Alert>
                <Link to={RoutePath.DASHBOARD}>
                    <Button variant="warning">Back</Button>
                </Link>
                <Container className="mt-4">
                    <Form onSubmit={this.add}>
                        <Form.Group controlId="formBasicName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter your name"
                                value={this.state.name}
                                onChange={(event) => this.setState({name: event.target.value})}
                                required
                            />
                        </Form.Group>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                                type="email"
                                placeholder="Enter email" 
                                value={this.state.email}
                                onChange={(event) => this.setState({email: event.target.value})}
                                required
                            />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Enter password" 
                                value={this.state.password}
                                onChange={(event) => this.setState({password: event.target.value})}
                                required
                            />
                        </Form.Group>
                        <Form.Group controlId="formBasicAddress">
                            <Form.Label>Address</Form.Label>
                            <Form.Control
                                as="textarea"
                                placeholder="Enter your address"
                                rows={7}
                                value={this.state.address}
                                onChange={(event) => this.setState({address: event.target.value})}
                                required
                            />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Form>
                </Container>
            </>
        )
    }
}

export default Register;