import axios from "axios";
import React, { useState } from 'react';
import { Component } from "react";
import { Alert, Button, Container, Form } from "react-bootstrap";
import { Route , withRouter, Link, Redirect } from "react-router-dom";
import RoutePath from "../../RoutePath";
import { setUserSession } from '../../Utils/Common';
import './Login.css';
import { getToken, getUser} from '../../Utils/Common';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentView: "logIn"
    }
  }

  state = { clicked: false }

  changeView = (view) => {
    this.setState({
      currentView: view
    })
  }

  add = event => {
    event.preventDefault()

      let data = {
          email: this.state.email,
          password: this.state.password
      }

      axios.post(`${process.env.REACT_APP_BASE_API_URL}auth/login`, data)
      .then(response => {
          setUserSession(response.data.data.access_token, response.data.data);
          window.location.replace(RoutePath.DASHBOARD);
          // this.props.history.push(RoutePath.DASHBOARD);
        })
      .catch(error => {
        alert("User Not Found!");
        console.log(error)
      });
      
  }

  handelRegister = (event) => {
    event.preventDefault()

    let data = {
        name: this.state.name,
        email: this.state.email,
        password: this.state.password,
        address: this.state.address,
    }

    if (
        !this.state.name ||
        !this.state.email ||
        !this.state.password ||
        !this.state.address
        ) alert("mohon isi lengkap form")

    axios.post(`${process.env.REACT_APP_BASE_API_URL}auth/register`, data)
    .then(response => {
        setUserSession(response.data.data.access_token, response.data.data);
        this.setState({
            name: '',
            email: '',
            password: '',
            address: '',
        })
        window.location.replace(RoutePath.DASHBOARD);
      }).catch(error => {
        console.log(error)
    })

}

  currentView = () => {
    switch(this.state.currentView) {
      case "signUp":
        return (
          <form className="email-signup" onSubmit={this.handelRegister}>
            <div className="u-form-group">
            <input 
              type="name" 
              placeholder="Name"
              value={this.state.name}
              onChange={(event) => this.setState({name: event.target.value})}
              required
              />
            </div>
            <div className="u-form-group">
            <input 
              type="address" 
              placeholder="Address"
              value={this.state.address}
              onChange={(event) => this.setState({address: event.target.value})}
              required
              />
            </div>
            <div className="u-form-group">
            <input 
              type="email" 
              placeholder="Email"
              value={this.state.email}
              onChange={(event) => this.setState({email: event.target.value})}
              required
              />
            </div>
            <div className="u-form-group">
              <input 
              type="password" 
              placeholder="Password"
              value={this.state.password}
              onChange={(event) => this.setState({password: event.target.value})}
              required
              />
            </div>
            <div className="u-form-group">
              <button>Sign Up</button>
            </div>
          </form>
        )
        break
      case "logIn":
        return (
          <form className="email-login" onSubmit={(event) => this.add(event)}>
            <div className="u-form-group">
              <input 
              type="email" 
              placeholder="Email"
              onChange={(event) => this.setState({email: event.target.value})}
              required
              />
            </div>
            <div className="u-form-group">
              <input 
              type="password" 
              placeholder="Password"
              onChange={(event) => this.setState({password: event.target.value})}
              required
              />
            </div>
            <div className="u-form-group">
              <button type="submit">Log in</button>
            </div>
          </form>
        )
        break
      default:
        break
    }
  }

  render() {

    if(getToken()){
      return <Redirect to = {RoutePath.DASHBOARD} />
    }

    return (
      <>
        <div className="login-box">
          <div className="lb-header">
            <a 
              className={this.state.currentView == "logIn"? 'active' : ''} 
              id="login-box-link"
              onClick={ () => this.changeView("logIn")}
              >Login</a>
            <a 
              id="signup-box-link" 
              className={this.state.currentView == "signUp"? 'active' : ''} 
              onClick={ () => this.changeView("signUp")}
            >Sign Up</a>
          
          </div>
          {this.currentView()}
          {/* {this.state.clicked ? <RegisterForm/> : <LoginForm/>} */}
        </div>
      </>
    )
  }

}

export default withRouter(Login);