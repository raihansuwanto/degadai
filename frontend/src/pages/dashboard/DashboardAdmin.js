import { Component } from "react";
import { Link } from "react-router-dom";
import { Alert, Button, Col, Container, Row } from "react-bootstrap";
import RoutePath from "../../RoutePath";
import {     removeUserSession,getToken,getUser } from '../../Utils/Common';
import Navbar from "../../Navbar/Navbar"

const user = getUser();
const token = getToken();
const handleLogout = () => {
    removeUserSession();
  }

class DashboardAdmin extends Component {
    render() {
        return (
            <>
                <Navbar />
                <Alert variant="primary" className="text-center">Ticket Landing Page</Alert>
                <Container>
                    <Row>
                        <Col md={12}>
                            <Link to={RoutePath.DASHBOARD}>
                                <Button onClick={handleLogout} variant="warning">Logout</Button>
                            </Link>
                        </Col>
                        <Col md={12} className="mt-5">
                            <Link to={RoutePath.TICKET_INDEX}>
                                <Button variant="success">Ticket Manajemen</Button>
                            </Link>
                        </Col>
                        <Col md={12} className="mt-5">
                            <Link to={RoutePath.CUSTOMER_ORDER}>
                                <Button variant="success">Order Manajemen</Button>
                            </Link>
                        </Col>
                    </Row>
                </Container>
            </>
        )
    }
}

export default DashboardAdmin;