import { Component } from "react";
import { Link } from "react-router-dom";
import { Alert, Button, Col, Container, Row } from "react-bootstrap";
import RoutePath from "../../RoutePath";
import {     removeUserSession,getToken,getUser } from '../../Utils/Common';

const user = getUser();
const token = getToken();
const handleLogout = () => {
    removeUserSession();
  }

class DashboardCustomer extends Component {
    render() {
        return (
            <>
                <Alert variant="primary" className="text-center">Ticket Landing Page</Alert>
                <Container>
                    <Row>
                        <Col md={12}>
                            <Link to={RoutePath.DASHBOARD}>
                                <Button onClick={handleLogout} variant="warning">Logout</Button>
                            </Link>
                        </Col>
                        <Col md={12} className="mt-5">
                            <Link to={RoutePath.TICKET_CUSTOMER_INDEX}>
                                <Button variant="success">My Order</Button>
                            </Link>
                        </Col>
                    </Row>
                </Container>
            </>
        )
    }
}

export default DashboardCustomer;