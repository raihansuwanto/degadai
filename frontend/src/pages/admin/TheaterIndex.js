import axios from "axios";
import {  Table } from "react-bootstrap";
import { getToken } from '../../Utils/Common';
import React, { useState, useEffect, useMemo, useRef } from "react";
import Template from "../../components/template/Template";
import "./TheaterIndex.css"
import { Theatre } from '../../components/container/theatre'

const TheaterIndex = props => {

const token = getToken();
const [theater, setTheater] = useState([]);
const [searchNama, setSearchName] = useState("");

const onSubmit = (event) => {
  console.log(event.target.name.value);
  let data = {
    name: event.target.name.value,
    address: event.target.address.value,
    telp: event.target.telp.value,
    area_id: parseInt(event.target.area.value)
  }

  axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  axios.post(`${process.env.REACT_APP_BASE_API_URL}theater`, data)
  .then(response => {
      alert("data berhasil ditambahkan")
      event.history.push("/theater-index");
  }).catch(error => {
      console.log(error)
  })
}

useEffect(() => {
  retrieveTheaters();
}, []);

  const retrieveTheaters = () => {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
    axios.get(`${process.env.REACT_APP_BASE_API_URL}theater`, {
    })
    .then(response => {
      setTheater(response.data.data);
    })
    .catch(error => {
        console.log(error)
    });
  };

const findByName = e => {
    const searchTitle = e.target.value;
        axios.get(`${process.env.REACT_APP_BASE_API_URL}ticket?name=${searchNama}`, {
        }).then(response => {
            setTheater(response.data.data);
            console.log(response.data.data)
        }).catch(error => {
            console.log(error)
        })
}

const onChangeSearchName = (e) => {
  const searchName = e.target.value;
  setSearchName(searchName);
};

const handleClick = (id) => {
  window.location.replace(`theater/${id}`);
}

        return (
          <>
          <Template>
          <br/>
          <br/>
          <div className="col-md-8">
            <div className="input-group mb-3">
              <input
                type="text"
                className="form-control"
                placeholder="Search by title"
                value={searchNama}
                onChange={onChangeSearchName}
              />
              <div className="input-group-append">
                <button
                  className="btn btn-secondary"
                  type="button"
                  onClick={findByName}
                >
                  Search
                </button>
              </div>
            </div>
          </div>

          <div className="container-table">
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Area</th>
                <th>Address</th>
                <th>Telp</th>
              </tr>
            </thead>
            <tbody>            {
              theater.map((post) =>
                <tr>
                  
                  <td className="td-table">{post.ID}</td>
                  <td className="td-table"><a value={post.ID} onClick={() => handleClick(post.ID)}>{post.name}</a></td>
                  <td className="td-table">{post.area.name}</td>
                  <td className="td-table">{post.address}</td>
                  <td className="td-table">{post.telp}</td>
                  
                </tr>
              )
            }
            </tbody>
          </Table>
          <Theatre modalId="1" triggerText="Add Theatre" onSubmit={onSubmit} />
          </div>
          </Template>
          </>
        );
}

export default TheaterIndex;