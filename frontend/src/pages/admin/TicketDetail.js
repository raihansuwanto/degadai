import axios from "axios";
import { Component } from "react";
import { Alert, Figure, Button, Col, Container, Form, Row, FormGroup } from "react-bootstrap";
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import RoutePath from "../../RoutePath";
import { getToken } from '../../Utils/Common';
import { useTable } from "react-table";
import Navbar from "../../Navbar/Navbar"
import "./Ticket.css"
import FormFileInput from "react-bootstrap/esm/FormFileInput";
import Template from "../../components/template/Template";

const TicketDetail = props => {
    const initialTicketState = {
      id: null,
      name: "",
      description: "",
      stock: "",
      image: null
    };
// }

const token = getToken();
const [currentTicket, setCurrentTicket] = useState(initialTicketState);
const [message, setMessage] = useState("");
const image = currentTicket.image
              ?`${process.env.REACT_APP_IMAGE_URL}`+ currentTicket.image
              : RoutePath.DEFAULT_IMAGE;

const [file, setFile] = useState();
const [uploadedFile, setUploadedFile] = useState();

useEffect(() => {
  getTicket(props.match.params.id);
}, [props.match.params.id]);

const handleInputChange = event => {
  const { name, value } = event.target;
  setCurrentTicket({ ...currentTicket, [name]: value });
};

const getTicket = id => {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
    axios.get(`${process.env.REACT_APP_BASE_API_URL}ticket/${id}`, {
    })
    .then(response => {
        setCurrentTicket(response.data.data);
        console.log(response.data.data);
    })
    .catch(error => {
        console.log(error)
    });
  };

  const updateTicket = e=> {
    e.preventDefault();
    // console.log(file);
    const formData = new FormData();
    formData.append('image',file);    
    console.log(formData)
    
    // let data = {
    //   name: currentTicket.name,
    //   description: currentTicket.description,
    //   stock: currentTicket.stock,
    //   // image: this.state.image
    // }

    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
    axios.put(`${process.env.REACT_APP_BASE_API_URL}ticket/${currentTicket.ID}`, {
      formData
      // name: currentTicket.name,
      // description: currentTicket.description,
      // stock: currentTicket.stock
      // image
    })
      .then(response => {
        console.log(response.data);
        // setMessage("The tutorial was updated successfully!");
      })
      .catch(e => {
        console.log(e);
      });
  };

  const handleImageChange = (e) => {
    e.preventDefault();
    let reader = new FileReader();
    let thefile = e.target.files[0];
    reader.onloadend = () => {
      setFile(thefile)
      setUploadedFile(reader.result)
    }
    reader.readAsDataURL(thefile)
  }


    // render() {
        return (
          <>
          <Template>

          <Container>
            <Row>
              <Col sm={5}/>
              <Col sm={6}>
              <Figure className="image">
                  <Figure.Image
                    width={171}
                    height={180}
                    alt="171x180"
                    src={image}
                  />
                </Figure>
              </Col>
            </Row>
            <Row>
              <Col sm={2}/> 
              <Col sm={8}>
              {/* <Form onSubmit={updateTicket}> */}
                <div className="edit-form">
                  <form>
                    <div className="form-group">
                    <label htmlFor="name">Name</label>
                      <input
                        type="text"
                        className="form-control"
                        id="name"
                        name="name"
                        value={currentTicket.name}
                        onChange={handleInputChange}
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="description">Description</label>
                      <input
                        type="text"
                        className="form-control"
                        id="description"
                        name="description"
                        value={currentTicket.description}
                        onChange={handleInputChange}
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="description">Stock</label>
                      <input
                        type="text"
                        className="form-control"
                        id="stock"
                        name="stock"
                        value={currentTicket.stock}
                        onChange={handleInputChange}
                      />
                    </div>
                    </form>
                    <Form onSubmit={updateTicket} encType="multipart/form-data">
                    <div className="form-group">
                      <label htmlFor="description">Image</label>
                      <Form.File
                        type="file"
                        className="form-control"
                        id="image"
                        name="image"
                        onChange={handleInputChange}
                      />
                    </div>
                    </Form>
    
          <Form onSubmit={updateTicket} encType="multipart/form-data">
            <div>
              <FormGroup className="style_file_input">
                <FormFileInput type="file" accept="image/*" id="exampleCustomFileBrowser" onChange={(e)=> handleImageChange(e)} />
              </FormGroup>
            </div>
              <Button className="style_photo_button"
                color="primary"
                type="submit"
                disabled={ uploadedFile == null || file == null }
              >
                Update Photo
              </Button>
        </Form>
                  
                
                  <Button className="badge badge-danger mr-2">
                    Delete
                  </Button>
        
                  <Button
                    type="submit"
                    className="badge badge-success"
                    onClick={updateTicket}
                  >
                    Update
                  </Button>
                  <p>{message}</p>
                </div>
                {/* </Form> */}
              </Col>
            </Row>
          </Container>

            </Template>            
          </>
          );
        }
    // }

export default TicketDetail;